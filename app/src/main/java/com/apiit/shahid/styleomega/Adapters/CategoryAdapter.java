package com.apiit.shahid.styleomega.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apiit.shahid.styleomega.CategoryActivity;
import com.apiit.shahid.styleomega.Models.Tag;
import com.apiit.shahid.styleomega.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shahid on 5/18/2018.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder>{


    private List<Tag> tagList;
    private Context mContext;

    public CategoryAdapter(List<Tag> tagList, Context context) {
        this.tagList = tagList;
        mContext = context;
    }

    @NonNull
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View customView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_category ,parent,false);
        return new CategoryAdapter.ViewHolder(customView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.ViewHolder holder, int position) {
        final Tag tagItem = tagList.get(position);
        holder.mName.setText(tagItem.getProductTag());
        Picasso.get().load(tagItem.getTagImage()).placeholder(R.drawable.placeholder_img).into(holder.mImage);
        holder.mTagLayout.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent mNewIntent = new Intent(mContext, CategoryActivity.class);
                mNewIntent.putExtra("mSelectedTag",tagItem.getId());
                mContext.startActivity(mNewIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tagList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tagName) TextView mName;
        @BindView(R.id.categoryImage) ImageView mImage;
        @BindView(R.id.tagLayout) RelativeLayout mTagLayout;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


}
