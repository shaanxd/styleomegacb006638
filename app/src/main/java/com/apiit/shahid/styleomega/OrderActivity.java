package com.apiit.shahid.styleomega;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Adapters.PurchasedAdapter;
import com.apiit.shahid.styleomega.Models.Cart;
import com.apiit.shahid.styleomega.Models.Orders;
import com.apiit.shahid.styleomega.Models.Product;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderActivity extends BaseActivity {


    @BindView(R.id.userAddress) TextView mAddress;
    @BindView(R.id.userCity) TextView mCity;
    @BindView(R.id.userProvince) TextView mProvince;
    @BindView(R.id.cardNumber) TextView mCardNumber;
    @BindView(R.id.cancelBtn) Button mCancelBtn;
    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;

    private Orders mSelectedOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        setupActionBar();
        ButterKnife.bind(this);
        mSelectedOrder = Orders.findById(Orders.class, getIntent().getLongExtra("mSelectedOrder",0));
        setTitle("Order #"+mSelectedOrder.getId());

        mAddress.setText(mSelectedOrder.getAddress());
        mCity.setText(mSelectedOrder.getCity());
        mProvince.setText(mSelectedOrder.getProvince());
        mCardNumber.setText(mSelectedOrder.getCardNumber());
        mRecyclerView.setAdapter(new PurchasedAdapter(mSelectedOrder.getItemList(), this));
        if(mSelectedOrder.isCancelled()){
            mCancelBtn.setText(R.string.re_order);
        }
        else{
            mCancelBtn.setText(R.string.cancel_order);
        }
    }

    @OnClick(R.id.cancelBtn)
    public void cancelOrder(View mView){
        if(mSelectedOrder.isCancelled()){
            if(checkItems()){
                mSelectedOrder.setCancelled(false);
                mSelectedOrder.save();
                for (Cart mItem : mSelectedOrder.getItemList()) {
                    Product mProduct = Product.findById(
                            Product.class,
                            mItem.getProduct().getId());
                    mProduct.setAvailableStock(
                            mProduct.getAvailableStock() - mItem.getQuantity());
                    mProduct.save();
                }
                Toast.makeText(this, R.string.re_order_successfull, Toast.LENGTH_SHORT).show();
                finish();
            }
            else{
                Toast.makeText(this, R.string.unavailable_stock, Toast.LENGTH_SHORT).show();
            }
        }
        else {
            mSelectedOrder.setCancelled(true);
            mSelectedOrder.save();
            for (Cart mItem : mSelectedOrder.getItemList()) {
                Product mProduct = Product.findById(
                        Product.class,
                        mItem.getProduct().getId());
                mProduct.setAvailableStock(
                        mItem.getQuantity() + mProduct.getAvailableStock());
                mProduct.save();
            }
            finish();
        }
    }

    public boolean checkItems(){
        for(Cart mItem : mSelectedOrder.getItemList()){
            Product mProduct = Product.findById(Product.class, mItem.getProduct().getId());
            if(mProduct.getAvailableStock()<mItem.getQuantity()){
                return false;
            }
        }
        return true;
    }
}
