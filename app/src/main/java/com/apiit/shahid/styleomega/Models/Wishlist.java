package com.apiit.shahid.styleomega.Models;

import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.io.Serializable;

/**
 * Created by Shahid on 4/24/2018.
 */

public class Wishlist extends SugarRecord implements Serializable {

    Product mProduct;
    User mUser;

    public Wishlist() {
    }

    public Wishlist(Product product, User user) {
        mProduct = product;
        mUser = user;
    }

    public Product getProduct() {
        return mProduct;
    }

    public User getUser() {
        return mUser;
    }

    public static Wishlist checkWishlist(Product mSelectedProduct, User mLoggedUser){
        return Select.from(Wishlist.class)
                .where(Condition.prop("m_product").eq(mSelectedProduct.getId()))
                .and(Condition.prop("m_user").eq(mLoggedUser.getId())).first();
    }

}
