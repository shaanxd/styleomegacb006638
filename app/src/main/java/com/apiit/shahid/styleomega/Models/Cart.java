package com.apiit.shahid.styleomega.Models;

import android.support.annotation.Nullable;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by Shahid on 4/22/2018.
 */

public class Cart extends SugarRecord implements Serializable{

    private Product mProduct;
    private User mUser;
    private int mQuantity;
    private double mTotalPrice;
    private Size mSize;
    private boolean mIsPurchased;
    @Nullable
    private Orders mOrder;

    public Cart() {
    }

    public Cart(Product product, User user, int quantity, Size size) {
        mProduct = product;
        mUser = user;
        mQuantity = quantity;
        mTotalPrice = quantity*product.getActualPrice();
        mSize = size;
    }

    public Orders getOrder() {
        return mOrder;
    }

    public void setOrder(Orders order) {
        mOrder = order;
    }

    public Product getProduct() {
        return mProduct;
    }

    public User getUser() {
        return mUser;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public double getTotalPrice() {
        return mTotalPrice;
    }

    public Size getSize() {
        return mSize;
    }

    public boolean isPurchased() {
        return mIsPurchased;
    }

    public void setPurchased(boolean purchased) {
        mIsPurchased = purchased;
    }

    public void setQuantity(int quantity) {
        mQuantity = quantity;
    }
}
