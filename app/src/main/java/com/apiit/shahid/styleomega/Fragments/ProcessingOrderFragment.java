package com.apiit.shahid.styleomega.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apiit.shahid.styleomega.Adapters.OrderAdapter;
import com.apiit.shahid.styleomega.Models.User;
import com.apiit.shahid.styleomega.R;
import com.apiit.shahid.styleomega.StyleOmega;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shahid on 5/30/2018.
 */

public class ProcessingOrderFragment extends BaseFragment{

    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;

    private User mCurrentUser;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_processing_orders, container, false);
        ButterKnife.bind(this, view);
        mCurrentUser = StyleOmega.getSharedPreference(mContext);
        initRecycler();
        return view;
    }

    public void initRecycler(){
        mRecyclerView.setAdapter(new OrderAdapter(
                mCurrentUser.getOrdersList(false), mContext));
    }

}
