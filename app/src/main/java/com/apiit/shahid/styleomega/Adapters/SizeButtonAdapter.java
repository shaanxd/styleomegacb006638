package com.apiit.shahid.styleomega.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.apiit.shahid.styleomega.Models.Size;
import com.apiit.shahid.styleomega.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shahid on 4/30/2018.
 */

public class SizeButtonAdapter extends RecyclerView.Adapter<SizeButtonAdapter.ViewHolder>{


    private int lastCheckedPosition = -1;
    private Size mSelectedSize = null;
    private List<Size> sizeList;
    private Context mContext;

    public SizeButtonAdapter(List<Size> sizeList, Context context) {
        this.sizeList = sizeList;
        mContext = context;
    }

    @NonNull
    @Override
    public SizeButtonAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View customView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_size_button_layout,parent,false);
        return new SizeButtonAdapter.ViewHolder(customView);
    }

    @Override
    public void onBindViewHolder(@NonNull SizeButtonAdapter.ViewHolder holder, int position) {
        final Size sizeItem = sizeList.get(position);
        holder.mRadioButton.setText(sizeItem.getProductSize());
        boolean isPosition = position == lastCheckedPosition;
        holder.mRadioButton.setChecked(isPosition);
        if(isPosition){
            mSelectedSize = sizeList.get(position);
        }
    }

    @Override
    public int getItemCount() {
        return sizeList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.radioButton) RadioButton mRadioButton;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mRadioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastCheckedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }

    }

    public Size getSize(){
        return mSelectedSize;
    }


}