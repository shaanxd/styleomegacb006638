package com.apiit.shahid.styleomega.Models;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Shahid on 4/18/2018.
 */
public class Product extends SugarRecord implements Serializable{
    private String productCode;
    private String productName;
    private String productDesc;
    private String scaledImg;
    private int availableStock;
    private double actualPrice;
    private int productDiscount;
    private boolean newArrival;

    @Ignore
    private List<String> productTags;
    @Ignore
    private List<String> productImages;

    public Product(){

    }

    public Product(String productCode, String productName, String productDesc, String scaledImg, int availableStock, double actualPrice, int productDiscount, boolean newArrival){
        this.productCode = productCode;
        this.productName = productName;
        this.productDesc = productDesc;
        this.scaledImg = scaledImg;
        this.availableStock = availableStock;
        this.actualPrice = actualPrice;
        this.productDiscount = productDiscount;
        this.newArrival = newArrival;
    }


    public List<String> getProductTags() {
        return productTags;
    }

    public List<String> getImageList() {
        return productImages;
    }

    public void setAvailableStock(int availableStock) {
        this.availableStock = availableStock;
    }

    public String getProductCode(){
        return productCode;
    }

    public String getProductName() {
        return productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public int getAvailableStock() {
        return availableStock;
    }

    public double getActualPrice() {
        return actualPrice;
    }

    public int getProductDiscount() {
        return productDiscount;
    }

    public String getScaledImg() {
        return scaledImg;
    }

    public boolean isNewArrival() {
        return newArrival;
    }

    public List<ProductTag> getTagList(){
        return ProductTag.find(ProductTag.class, "m_product = ?", this.getId().toString());
    }

    public Tag getFirstTag(){
        return Select.from(ProductTag.class).where(Condition.prop("m_product").eq(this.getId().toString())).first().getProductTag();
    }

    public List<Size> getSizeList(){
        return Size.listAll(Size.class);
    }

    public List<Inquiry> getInquiryList(Boolean isLimited){
        List<Inquiry> mInquiryList;
        if(isLimited){
            mInquiryList = Select.from(Inquiry.class).where(Condition.prop("m_product").eq(this.getId().toString())).limit("3").list();
        }
        else{
            mInquiryList = Select.from(Inquiry.class).where(Condition.prop("m_product").eq(this.getId().toString())).list();
        }
        return mInquiryList;
    }

    public List<Review> getReviewList(Boolean isLimited){
        List<Review> mReviewList;
        if(isLimited) {
            mReviewList = Select.from(Review.class).where(Condition.prop("m_product").eq(this.getId().toString())).limit("3").list();
        }
        else{
            mReviewList = Select.from(Review.class).where(Condition.prop("m_product").eq(this.getId().toString())).list();
        }
        return mReviewList;
    }

    public float getAverageRating(){
        List<Review> mReviewList = getReviewList(false);
        float mTotalRating = 0;

        for(Review mReview: mReviewList){
            mTotalRating += mReview.getRating();
        }

        float mRating =  mTotalRating/mReviewList.size();

        if(Float.isNaN(mRating)){
            return 0.0f;
        }

        BigDecimal mBigDecimal = new BigDecimal(mRating);
        mBigDecimal = mBigDecimal.setScale(1, BigDecimal.ROUND_HALF_UP);

        return  mBigDecimal.floatValue();
    }

    public int getRating(int mRatingNumber){
        long mRatingsUnder = Select.from(Review.class).where(Condition.prop("m_product")
                .eq(this.getId().toString())).and(Condition.prop("m_rating")
                .eq(String.valueOf(mRatingNumber))).count();

        long mTotalRatings = Select.from(Review.class).where(Condition.prop("m_product").eq(this.getId().toString())).count();

        if(mTotalRatings!=0) {

            long mAverageRating = (mRatingsUnder * 100) / mTotalRatings;
            return (int) mAverageRating;
        }
        return 0;
    }

    public static List<Product> getNewArrivals(){
        return Select.from(Product.class).where(Condition.prop("new_arrival").eq(1)).limit("4").list();
    }

    public static Product getProduct(String productCode){
        return Select.from(Product.class).where(Condition.prop("product_code").eq(productCode)).first();
    }

    public static List<Product> getLikeProducts(String productName){
        return Select.from(Product.class)
                .where(Condition.prop("product_name")
                        .like("%"+productName+"%")).list();
    }

    public static List<Product> getDiscountedItems(){
        return Select.from(Product.class).where(Condition.prop("product_discount").notEq(0)).limit("4").list();
    }

    public List<ProductImage> getProductImages(){
        return Select.from(ProductImage.class).where(Condition.prop("m_product").eq(this.getId())).list();
    }
}
