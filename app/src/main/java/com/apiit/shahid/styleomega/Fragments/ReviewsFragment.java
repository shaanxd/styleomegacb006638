package com.apiit.shahid.styleomega.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apiit.shahid.styleomega.Adapters.ReviewAdapter;
import com.apiit.shahid.styleomega.Models.Review;
import com.apiit.shahid.styleomega.Models.User;
import com.apiit.shahid.styleomega.R;
import com.apiit.shahid.styleomega.StyleOmega;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shahid on 5/21/2018.
 */

public class ReviewsFragment extends BaseFragment{

    @BindView(R.id.mRecyclerView) RecyclerView mRecyclerView;

    private User mCurrentUser;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reviews, container, false);
        ButterKnife.bind(this, view);
        mCurrentUser = StyleOmega.getSharedPreference(mContext);
        initRecyclerView();
        return view;
    }

    public void initRecyclerView(){
        List<Review> mReviewList = mCurrentUser.getReviews(false);
        mRecyclerView.setAdapter(new ReviewAdapter(mReviewList, mContext));
    }
}
