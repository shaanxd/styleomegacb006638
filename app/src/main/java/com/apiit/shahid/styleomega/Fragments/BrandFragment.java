package com.apiit.shahid.styleomega.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.apiit.shahid.styleomega.Adapters.CategoryAdapter;
import com.apiit.shahid.styleomega.Models.Tag;
import com.apiit.shahid.styleomega.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shahid on 5/18/2018.
 */

public class BrandFragment  extends BaseFragment{

    private static final int COLUMN_NUM = 2;

    @BindView(R.id.tagsRecyclerView) RecyclerView mRecyclerView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_brand, container, false);
        ButterKnife.bind(this, view);
        List<Tag> mTagList = Tag.getTagList("Brand");
        GridLayoutManager mGridLayout = new GridLayoutManager(mContext, COLUMN_NUM,
                LinearLayout.VERTICAL, false);
        CategoryAdapter mAdapter = new CategoryAdapter(mTagList, mContext);
        mRecyclerView.setLayoutManager(mGridLayout);
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }

}
