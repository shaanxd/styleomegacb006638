package com.apiit.shahid.styleomega.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.apiit.shahid.styleomega.Models.Cart;
import com.apiit.shahid.styleomega.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shahid on 5/21/2018.
 */

public class PurchasedAdapter extends RecyclerView.Adapter<PurchasedAdapter.ViewHolder>{


    private List<Cart> mPurchasedList;
    private Context mContext;

    public PurchasedAdapter(List<Cart> purchasedList, Context context) {
        this.mPurchasedList = purchasedList;
        mContext = context;
    }

    @NonNull
    @Override
    public PurchasedAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View customView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_purchased,parent,false);
        return new PurchasedAdapter.ViewHolder(customView);
    }

    @Override
    public void onBindViewHolder(@NonNull final PurchasedAdapter.ViewHolder holder, int position) {
        final Cart mCartItem = mPurchasedList.get(position);
        Picasso
                .get()
                .load(mCartItem.getProduct().getScaledImg())
                .placeholder(R.drawable.placeholder_img)
                .into(holder.mProductThumbnail);
        holder.mProductName.setText(mCartItem.getProduct().getProductName());

        String mQuantity = "x"+String.valueOf(mCartItem.getQuantity());
        holder.mProductQuantity.setText(mQuantity);
        holder.mTotalPrice.setText(String.valueOf(mCartItem.getTotalPrice()));
        holder.mSizeTag.setText(mCartItem.getSize().getProductSize());
        holder.mProductRating.setRating(mCartItem.getProduct().getAverageRating());
    }

    @Override
    public int getItemCount() {
        return mPurchasedList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.productSize) TextView mSizeTag;
        @BindView(R.id.productThumbnail) CircleImageView mProductThumbnail;
        @BindView(R.id.productName) TextView mProductName;
        @BindView(R.id.productQuantity) TextView mProductQuantity;
        @BindView(R.id.totalPrice) TextView mTotalPrice;
        @BindView(R.id.productRating) RatingBar mProductRating;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


}
