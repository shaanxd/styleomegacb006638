package com.apiit.shahid.styleomega;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Adapters.ProductAdapter;
import com.apiit.shahid.styleomega.Models.Product;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends BaseActivity {

    private static final int COLUMN_NUM = 2;

    @BindView(R.id.searchList) RecyclerView mSearchList;
    @BindView(R.id.recyclerLayout) LinearLayout mRecyclerLayout;

    List<Product> mProductList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        setupActionBar();
        setTitle(R.string.search_text);

        String mQuery = getIntent().getStringExtra("mSearchQuery");
        setSearchList(mQuery);
    }

    public void setSearchList(String query){
        mProductList = Product.getLikeProducts(query);
        GridLayoutManager mSearchLayout = new GridLayoutManager(
                this, COLUMN_NUM, LinearLayout.VERTICAL, false);
        mSearchList.setLayoutManager(mSearchLayout);
        mSearchList.setAdapter(new ProductAdapter(mProductList, this));
    }

    @OnClick(R.id.filterButton)
    public void onSort(View mView){
        final Dialog mDialog = new Dialog(SearchActivity.this);
        mDialog.setContentView(R.layout.dialog_sort);

        Button mButton = mDialog.findViewById(R.id.sortBtn);
        final RadioGroup mGroup = mDialog.findViewById(R.id.sortGroup);

        mButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View mView){
                if (mGroup.getCheckedRadioButtonId() != -1)
                {
                    RadioButton mButton = mGroup.findViewById(mGroup.getCheckedRadioButtonId());
                    switch (mButton.getId()){
                        case R.id.priceLow:
                            Collections.sort(mProductList, new Comparator<Product>() {
                                @Override
                                public int compare(Product productOne, Product productTwo) {
                                    return Double.compare(productOne.getActualPrice(),productTwo.getActualPrice());
                                }
                            });
                            break;
                        case R.id.priceHigh:
                            Collections.sort(mProductList, new Comparator<Product>() {
                                @Override
                                public int compare(Product productOne, Product productTwo) {
                                    return Double.compare(productTwo.getActualPrice(),productOne.getActualPrice());
                                }
                            });
                            break;
                        case R.id.rating:
                            Collections.sort(mProductList, new Comparator<Product>() {
                                @Override
                                public int compare(Product productOne, Product productTwo) {
                                    return Float.compare(productTwo.getAverageRating(), productOne.getAverageRating());
                                }
                            });
                            break;
                    }
                    mSearchList.setAdapter(new ProductAdapter(mProductList, SearchActivity.this));
                    mDialog.dismiss();
                }
                else{
                    Toast.makeText(SearchActivity.this, "Please select an option", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mDialog.show();
    }
}
