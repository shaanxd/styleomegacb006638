package com.apiit.shahid.styleomega.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Interface.DialogListener;
import com.apiit.shahid.styleomega.Models.Product;
import com.apiit.shahid.styleomega.Models.Review;
import com.apiit.shahid.styleomega.Models.User;
import com.apiit.shahid.styleomega.R;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Shahid on 5/25/2018.
 */

public class ReviewDialog extends Dialog {

    @BindView(R.id.reviewTitle) EditText mTitle;
    @BindView(R.id.reviewComment) EditText mComment;
    @BindView(R.id.reviewRating) RatingBar mRating;

    private Context mContext;
    private DialogListener mListener;
    private User mCurrentUser;
    private Product mSelectedProduct;

    public ReviewDialog(@NonNull Context context, User currentUser, Product selectedProduct, DialogListener dialogListener) {
        super(context);
        mContext = context;
        mListener = dialogListener;
        mCurrentUser = currentUser;
        mSelectedProduct = selectedProduct;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_rating);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.reviewCancelBtn)
    public void cancelReview(View mView){
        this.dismiss();
    }

    @OnClick(R.id.reviewSubmitBtn)
    public void submitReview(View mView){
        String mTitleString = mTitle.getText().toString();
        String mCommentString = mComment.getText().toString();
        float mActualRating = mRating.getRating();

        if(!(mTitleString.isEmpty()||mCommentString.isEmpty()||mActualRating==0)){
            Review mReview = new Review(mSelectedProduct, mCurrentUser, mTitleString,
                    mCommentString, mActualRating, new Date());
            mReview.save();
            mListener.setValues();
            this.dismiss();
        }
        else{
            Toast.makeText(mContext,R.string.empty_error, Toast.LENGTH_SHORT).show();
        }
    }
}
