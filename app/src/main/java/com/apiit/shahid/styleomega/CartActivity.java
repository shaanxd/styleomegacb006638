package com.apiit.shahid.styleomega;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Adapters.CartAdapter;
import com.apiit.shahid.styleomega.Dialog.CheckoutDialog;
import com.apiit.shahid.styleomega.Interface.AdapterListener;
import com.apiit.shahid.styleomega.Models.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CartActivity extends BaseActivity implements AdapterListener{

    @BindView(R.id.itemNum) TextView mItemNum;
    @BindView(R.id.cartRecyclerView) RecyclerView mCartView;
    @BindView(R.id.subTotal) TextView mTotalPrice;
    @BindView(R.id.checkOutBtn) Button mCheckoutButton;

    private User mLoggedUser;
    private CartAdapter mCartAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        setTitle(R.string.nav_cart);
        setupActionBar();
        mLoggedUser = StyleOmega.getSharedPreference(this);

    }
    private void initCartView(){
        mCartAdapter = new CartAdapter(mLoggedUser.getCartItemList(),
                this, this);
        mCartView.setAdapter(mCartAdapter);
        mCartView.setLayoutManager(new LinearLayoutManager(this));
        updateValues();
    }

    @Override
    public void updateValues() {
        int mItemCountVal = mCartAdapter.getItemCount();
        double mTotalPriceVal = mCartAdapter.getTotalPrice();
        mItemNum.setText(String.valueOf(mItemCountVal));
        String mTotal = "$"+String.valueOf(mTotalPriceVal);
        mTotalPrice.setText(mTotal);

        mCheckoutButton.setEnabled(mItemCountVal!=0);
    }

    @OnClick(R.id.checkOutBtn)
    public void checkOutDialog(){
        if(mCartAdapter.checkAvailability()) {
            Dialog mCheckoutDialog = new CheckoutDialog(this, mLoggedUser,
                    mCartAdapter.getItemCount(), mCartAdapter.getTotalPrice());
            mCheckoutDialog.setCanceledOnTouchOutside(false);
            mCheckoutDialog.show();
        }
        else{
            Toast.makeText(this, R.string.unavailable_stock, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initCartView();
    }
}
