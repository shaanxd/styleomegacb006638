package com.apiit.shahid.styleomega.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.apiit.shahid.styleomega.CheckoutActivity;
import com.apiit.shahid.styleomega.Models.User;
import com.apiit.shahid.styleomega.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Shahid on 5/25/2018.
 */

public class CheckoutDialog extends Dialog {

    @BindView(R.id.userAddress) EditText mAddress;
    @BindView(R.id.userCity) EditText mCity;
    @BindView(R.id.userProvince) EditText mProvince;
    @BindView(R.id.contactNumber) EditText mContactNumber;
    @BindView(R.id.cardNumber) EditText mCardNumber;
    @BindView(R.id.cardCrc) EditText mSecurityNumber;
    @BindView(R.id.expirationDate) EditText mExpirationDate;

    @BindView(R.id.rememberCard) CheckBox mRememberBox;

    private User mCurrentUser;
    private Context mContext;
    private int mItemCount;
    private double mTotalPrice;

    public CheckoutDialog(@NonNull Context context, User currentUser, int itemCount, double totalPrice) {
        super(context);
        mContext = context;
        mCurrentUser = currentUser;
        mItemCount = itemCount;
        mTotalPrice = totalPrice;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_checkout);
        ButterKnife.bind(this);

        if(!(mCurrentUser.getCardNumber().equals("N/A"))){
            mCardNumber.setText(mCurrentUser.getCardNumber());
            mSecurityNumber.setText(mCurrentUser.getSecurityNumber());
            mExpirationDate.setText(mCurrentUser.getExpirationDate());
        }
        if(!(mCurrentUser.getUserAddress().equals("N/A"))){
            mAddress.setText(mCurrentUser.getUserAddress());
            mCity.setText(mCurrentUser.getUserCity());
            mProvince.setText(mCurrentUser.getUserProvince());
            mContactNumber.setText(mCurrentUser.getContactNumber());
        }
    }

    @OnClick(R.id.cancelBtn)
    public void cancelCheckout(View mView){
        this.dismiss();
    }

    @OnClick(R.id.submitBtn)
    public void submitCheckout(View mView){
        String mAddressString = mAddress.getText().toString();
        String mCityString = mCity.getText().toString();
        String mProvinceString = mProvince.getText().toString();
        String mContactString = mContactNumber.getText().toString();
        String mNumberString = mCardNumber.getText().toString();
        String mCvcString = mSecurityNumber.getText().toString();
        String mExpirationString = mExpirationDate.getText().toString();

        if(!(mAddressString.isEmpty()||mCityString.isEmpty()||mProvinceString.isEmpty()||
                mContactString.isEmpty()||mNumberString.isEmpty()||mCvcString.isEmpty()||mExpirationString.isEmpty())){
            if(mRememberBox.isChecked()){
                mCurrentUser.setUserAddress(mAddressString);
                mCurrentUser.setUserCity(mCityString);
                mCurrentUser.setUserProvince(mProvinceString);
                mCurrentUser.setContactNumber(mContactString);
                mCurrentUser.setCardNumber(mNumberString);
                mCurrentUser.setSecurityNumber(mCvcString);
                mCurrentUser.setExpirationDate(mExpirationString);
                mCurrentUser.save();
            }
            Bundle mBundle = new Bundle();
            mBundle.putString("mAddress", mAddressString);
            mBundle.putString("mCity", mCityString);
            mBundle.putString("mProvince", mProvinceString);
            mBundle.putString("mContact", mContactString);
            mBundle.putString("mCardNumber", mNumberString);
            mBundle.putString("mExpirationDate", mExpirationString);
            mBundle.putInt("mItemCount", mItemCount);
            mBundle.putDouble("mTotalPrice", mTotalPrice);

            Intent mIntent = new Intent(mContext, CheckoutActivity.class);
            mIntent.putExtras(mBundle);
            mContext.startActivity(mIntent);
            this.dismiss();
        }
        else{
            Toast.makeText(mContext, R.string.empty_error, Toast.LENGTH_SHORT).show();
        }
    }
}
