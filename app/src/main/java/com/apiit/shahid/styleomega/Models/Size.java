package com.apiit.shahid.styleomega.Models;

import com.orm.SugarRecord;

/**
 * Created by Shahid on 4/28/2018.
 */

public class Size extends SugarRecord{

    private String productSize;

    public Size() {
    }

    public Size(String productSize) {
        this.productSize = productSize;
    }

    public String getProductSize() {
        return productSize;
    }

}
