package com.apiit.shahid.styleomega.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apiit.shahid.styleomega.Adapters.FragmentAdapter;
import com.apiit.shahid.styleomega.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shahid on 5/15/2018.
 */

public class CategoryFragment extends BaseFragment{

    @BindView(R.id.categoryTab) TabLayout mTabLayout;
    @BindView(R.id.categoryPager) ViewPager mViewPager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, view);

        FragmentAdapter mAdapter = new FragmentAdapter(getChildFragmentManager());
        mAdapter.addFragment(new MenFragment(),"Men");
        mAdapter.addFragment(new WomenFragment(), "Women");
        mAdapter.addFragment(new BrandFragment(), "Brands");

        mViewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        return view;
    }
}
