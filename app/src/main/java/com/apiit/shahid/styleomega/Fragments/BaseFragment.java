package com.apiit.shahid.styleomega.Fragments;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

/**
 * Created by Shahid on 5/8/2018.
 */

public abstract class BaseFragment extends Fragment{

    protected Context mContext;
    protected Activity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

        if(context instanceof Activity){
            mActivity = (Activity) context;
        }
    }

}
