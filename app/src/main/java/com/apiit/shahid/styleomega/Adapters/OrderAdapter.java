package com.apiit.shahid.styleomega.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apiit.shahid.styleomega.Models.Orders;
import com.apiit.shahid.styleomega.OrderActivity;
import com.apiit.shahid.styleomega.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shahid on 5/24/2018.
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder>{


    private List<Orders> mOrdersList;
    private Context mContext;

    public OrderAdapter(List<Orders> ordersList, Context context) {
        this.mOrdersList = ordersList;
        mContext = context;
    }

    @NonNull
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View customView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_order,parent,false);
        return new OrderAdapter.ViewHolder(customView);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderAdapter.ViewHolder holder, int position) {
        final Orders mOrderItem = mOrdersList.get(position);

        holder.mOrderNumber.setText(String.valueOf(mOrderItem.getId()));
        holder.mNumberOfItems.setText(String.valueOf(mOrderItem.getItems()));
        String mTotal = String.valueOf(mOrderItem.getTotalPrice())+"$";
        holder.mTotalPrice.setText(mTotal);
        holder.mOrderDate.setText(mOrderItem.getDate());
        if(mOrderItem.isCancelled()){
            holder.mOrderStatus.setText(R.string.order_cancel);
            holder.mOrderStatus.setTextColor(mContext.getResources().getColor(R.color.colorRed));
        }
        else{
            holder.mOrderStatus.setText(R.string.order_process);
            holder.mOrderStatus.setTextColor(mContext.getResources().getColor(R.color.colorOrange));
        }
        holder.mOrderLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mContext, OrderActivity.class);
                mIntent.putExtra("mSelectedOrder", mOrderItem.getId());
                mContext.startActivity(mIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mOrdersList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.orderNumber) TextView mOrderNumber;
        @BindView(R.id.itemNum) TextView mNumberOfItems;
        @BindView(R.id.totalPrice) TextView mTotalPrice;
        @BindView(R.id.orderStatus) TextView mOrderStatus;
        @BindView(R.id.orderLayout) LinearLayout mOrderLayout;
        @BindView(R.id.orderDate) TextView mOrderDate;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


}
