package com.apiit.shahid.styleomega.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.apiit.shahid.styleomega.Adapters.WishlistAdapter;
import com.apiit.shahid.styleomega.Models.User;
import com.apiit.shahid.styleomega.R;
import com.apiit.shahid.styleomega.StyleOmega;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shahid on 4/19/2018.
 */

public class WishlistFragment extends BaseFragment{

    private static final int COLUMN_NUM = 2;

    @BindView(R.id.wishlistView) RecyclerView wishListView;

    User mCurrentUser;
    WishlistAdapter mWishlistAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wishlist, container, false);
        ButterKnife.bind(this, view);
        mCurrentUser = StyleOmega.getSharedPreference(mContext);
        return view;
    }

    private void initCartView(){
        mWishlistAdapter = new WishlistAdapter(
                mCurrentUser.getWishlist(), mContext);
        GridLayoutManager mGridLayout = new GridLayoutManager(
                mContext, COLUMN_NUM, LinearLayout.VERTICAL, false);
        wishListView.setLayoutManager(mGridLayout);
        wishListView.setAdapter(mWishlistAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        initCartView();
    }

}
