package com.apiit.shahid.styleomega.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Adapters.SizeButtonAdapter;
import com.apiit.shahid.styleomega.Models.Cart;
import com.apiit.shahid.styleomega.Models.Product;
import com.apiit.shahid.styleomega.Models.Size;
import com.apiit.shahid.styleomega.Models.User;
import com.apiit.shahid.styleomega.R;
import com.travijuu.numberpicker.library.Enums.ActionEnum;
import com.travijuu.numberpicker.library.Interface.ValueChangedListener;
import com.travijuu.numberpicker.library.NumberPicker;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Shahid on 5/25/2018.
 */

public class CartDialog extends Dialog {

    @BindView(R.id.quantityPicker) NumberPicker mQuantityPicker;
    @BindView(R.id.totalPrice) TextView mTotalPrice;
    @BindView(R.id.sizeView) RecyclerView mSizeView;
    @BindView(R.id.availableStock) TextView mAvailableStock;

    private Context mContext;
    private User mCurrentUser;
    private Product mSelectedProduct;
    private SizeButtonAdapter mSizeButtonAdapter;

    public CartDialog(@NonNull Context context, User currentUser, Product selectedProduct) {
        super(context);
        mContext = context;
        mCurrentUser = currentUser;
        mSelectedProduct = selectedProduct;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_cart);
        ButterKnife.bind(this);

        mQuantityPicker.setMin(1);
        mQuantityPicker.setMax(10);
        mTotalPrice.setText(String.valueOf(mSelectedProduct.getActualPrice()));

        String availableStock = String.valueOf(mSelectedProduct.getAvailableStock())+" pieces available.";
        mAvailableStock.setText(availableStock);

        mQuantityPicker.setValueChangedListener(new ValueChangedListener() {
            @Override
            public void valueChanged(int value, ActionEnum action) {
                mTotalPrice.setText(String.valueOf(mSelectedProduct.getActualPrice()*value));
            }
        });

        mSizeButtonAdapter = new SizeButtonAdapter(mSelectedProduct.getSizeList(), mContext);
        mSizeView.setAdapter(mSizeButtonAdapter);
    }

    @OnClick(R.id.cancelBtn)
    public void cancelInquiry(View mView){
        this.dismiss();
    }

    @OnClick(R.id.submitBtn)
    public void submitInquiry(View mView){
        Size mSelectedSize = mSizeButtonAdapter.getSize();
        if(mSelectedSize!=null){
            if(mQuantityPicker.getValue()<=mSelectedProduct.getAvailableStock()) {
                Cart mCartItem = mCurrentUser.hasProduct(mSelectedProduct.getId(),
                        mSelectedSize.getId());
                if (mCartItem != null) {
                    mCartItem.setQuantity(mCartItem.getQuantity()
                            + mQuantityPicker.getValue());
                } else {
                    mCartItem = new Cart(mSelectedProduct, mCurrentUser,
                            mQuantityPicker.getValue(), mSelectedSize);
                }
                mCartItem.save();
                Toast.makeText(mContext, R.string.cart_successful, Toast.LENGTH_SHORT).show();
                this.dismiss();
            }
            else{
                Toast.makeText(mContext, R.string.insufficient_quantity, Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(mContext, R.string.select_size, Toast.LENGTH_SHORT).show();
        }
    }
}
