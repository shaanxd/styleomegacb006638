package com.apiit.shahid.styleomega;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Dialog.EmailDialog;
import com.apiit.shahid.styleomega.Models.User;
import com.orm.query.Condition;
import com.orm.query.Select;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity{


    @BindView(R.id.userID) EditText mUserID;
    @BindView(R.id.userPassword) EditText mUserPassword;

    String mUserString;
    String mPasswordString;

    @OnClick(R.id.registerBtn)
    public void submitRegister(View v) {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    }

    @OnClick(R.id.loginBtn)
    public void submitLogin(View v) {
        mUserString = mUserID.getText().toString();
        mPasswordString = mUserPassword.getText().toString();
        if(!(mUserString.isEmpty()|| mPasswordString.isEmpty())) {
            User user = Select.from(User.class)
                    .where(Condition.prop("m_user_id").eq(mUserString)).first();
            if(user!=null) {
                if(user.getUserPassword().equals(StyleOmega.encrypt(mPasswordString))) {
                    StyleOmega.putSharedPreference(LoginActivity.this, user);
                    startActivity(new Intent(
                            LoginActivity.this, HomeActivity.class));
                    finish();
                }
                else{
                    Toast.makeText(LoginActivity.this, R.string.password_invalid, Toast.LENGTH_SHORT).show();
                }
            }
            else{
                Toast.makeText(LoginActivity.this, R.string.user_invalid, Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(LoginActivity.this, R.string.empty_error, Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.resetButton)
    public void resetPassword(View mView){
        EmailDialog mEmailDialog = new EmailDialog(this);
        mEmailDialog.setCanceledOnTouchOutside(false);
        mEmailDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

}
