package com.apiit.shahid.styleomega.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.apiit.shahid.styleomega.Models.Wishlist;
import com.apiit.shahid.styleomega.ProductActivity;
import com.apiit.shahid.styleomega.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shahid on 4/24/2018.
 */

public class WishlistAdapter extends RecyclerView.Adapter<WishlistAdapter.ViewHolder>{


    private List<Wishlist> mWishlistItems;
    private Context mContext;

    public WishlistAdapter(List<Wishlist> mWishlistItems, Context context) {
        this.mWishlistItems = mWishlistItems;
        mContext = context;
    }

    @NonNull
    @Override
    public WishlistAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View customView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_wishlist_layout,parent,false);
        return new WishlistAdapter.ViewHolder(customView);
    }

    @Override
    public void onBindViewHolder(@NonNull final WishlistAdapter.ViewHolder holder, int position) {
        final Wishlist wishlistItem = mWishlistItems.get(position);
        Picasso.get().load(wishlistItem.getProduct().getScaledImg()).placeholder(R.drawable.placeholder_img).into(holder.productThumbnail);
        holder.productName.setText(wishlistItem.getProduct().getProductName());
        holder.wishlistToggle.setChecked(true);
        holder.wishlistToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    wishlistItem.delete();
                    mWishlistItems.remove(wishlistItem);
                    notifyItemRemoved(holder.getAdapterPosition());
                }
            }
        });
        holder.mLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent productIntent = new Intent(mContext, ProductActivity.class);
                productIntent.putExtra("selectedProduct", wishlistItem.getProduct().getId());
                mContext.startActivity(productIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mWishlistItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.productThumbnail) ImageView productThumbnail;
        @BindView(R.id.productName) TextView productName;
        @BindView(R.id.wishlistToggle) ToggleButton wishlistToggle;
        @BindView(R.id.wishlistLayout) RelativeLayout mLayout;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
