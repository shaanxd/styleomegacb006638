package com.apiit.shahid.styleomega.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.apiit.shahid.styleomega.Adapters.ProductAdapter;
import com.apiit.shahid.styleomega.Adapters.SwipeAdapter;
import com.apiit.shahid.styleomega.Models.Product;
import com.apiit.shahid.styleomega.R;
import com.apiit.shahid.styleomega.Views.CustomViewPager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shahid on 4/19/2018.
 */

public class HomeFragment extends BaseFragment {

    private static final int COLUMN_NUM = 2;

    @BindView(R.id.newArrivalsGrid) RecyclerView mNewProductsGrid;
    @BindView(R.id.discountsGrid) RecyclerView mDiscountsGrid;
    @BindView(R.id.swipePager) CustomViewPager mCustomViewPager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        generateGrid();
        return view;
    }

    public void generateGrid(){
        List<Product> mNewList = Product.getNewArrivals();
        ProductAdapter mNewAdapter = new ProductAdapter(mNewList, mContext);
        GridLayoutManager mNewLayout = new GridLayoutManager(mContext, COLUMN_NUM,
                LinearLayout.VERTICAL, false);
        mNewProductsGrid.setLayoutManager(mNewLayout);
        mNewProductsGrid.setAdapter(mNewAdapter);
        mNewProductsGrid.setNestedScrollingEnabled(false);

        List<Product> mDiscountList = Product.getDiscountedItems();
        ProductAdapter mDiscountAdapter = new ProductAdapter(mDiscountList, mContext);
        GridLayoutManager mDiscountLayout = new GridLayoutManager(mContext, COLUMN_NUM,
                LinearLayout.VERTICAL, false);
        mDiscountsGrid.setLayoutManager(mDiscountLayout);
        mDiscountsGrid.setAdapter(mDiscountAdapter);
        mDiscountsGrid.setNestedScrollingEnabled(false);

        mCustomViewPager.setAdapter(new SwipeAdapter(mContext));
    }
}
