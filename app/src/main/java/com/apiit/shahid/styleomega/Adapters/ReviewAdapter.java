package com.apiit.shahid.styleomega.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.apiit.shahid.styleomega.Models.Review;
import com.apiit.shahid.styleomega.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shahid on 5/1/2018.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder>{

    private List<Review> mReviewList;
    private Context mContext;

    public ReviewAdapter(List<Review> mReviewList, Context context) {
        this.mReviewList = mReviewList;
        mContext = context;
    }

    @NonNull
    @Override
    public ReviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View customView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_product_review,parent,false);
        return new ReviewAdapter.ViewHolder(customView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewAdapter.ViewHolder holder, int position) {
        final Review mReview = mReviewList.get(position);
        holder.mUsername.setText(mReview.getUser().getUserFirstName());
        holder.mReviewSubject.setText(mReview.getTitle());
        holder.mReviewComment.setText(mReview.getComment());
        holder.mRatingCard.setRating(mReview.getRating());
        holder.mDate.setText(mReview.getDate());
        Picasso.get().load(mReview.getProduct().getScaledImg()).placeholder(R.drawable.placeholder_img).into(holder.mProduct);
    }

    @Override
    public int getItemCount() {
        return mReviewList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.reviewRating) RatingBar mRatingCard;
        @BindView(R.id.reviewSubject) TextView mReviewSubject;
        @BindView(R.id.reviewComment) TextView mReviewComment;
        @BindView(R.id.userName) TextView mUsername;
        @BindView(R.id.reviewProduct) CircleImageView mProduct;
        @BindView(R.id.reviewDate) TextView mDate;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
