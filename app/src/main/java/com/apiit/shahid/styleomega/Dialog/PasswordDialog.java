package com.apiit.shahid.styleomega.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Models.User;
import com.apiit.shahid.styleomega.R;
import com.apiit.shahid.styleomega.StyleOmega;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Shahid on 5/25/2018.
 */

public class PasswordDialog extends Dialog {

    @BindView(R.id.oldPassword) TextView mOldPassword;
    @BindView(R.id.newPassword) TextView mNewPassword;
    @BindView(R.id.newPasswordRe) TextView mNewPasswordRepeat;

    private User mCurrentUser;
    private Context mContext;

    public PasswordDialog(@NonNull Context context, User currentUser) {
        super(context);
        mContext = context;
        mCurrentUser = currentUser;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_update_password);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.cancelBtn)
    public void cancelUpdate(View mView){
        this.dismiss();
    }

    @OnClick(R.id.submitBtn)
    public void submitUpdate(View mView){
        String mOldString = mOldPassword.getText().toString();
        String mNewString = mNewPassword.getText().toString();
        String mNewRepeatString = mNewPasswordRepeat.getText().toString();

        if(mOldString.isEmpty()||mNewString.isEmpty()||mNewRepeatString.isEmpty()){
            Toast.makeText(mContext,R.string.empty_error,Toast.LENGTH_SHORT).show();
        }
        else{
            if(mNewString.equals(mNewRepeatString)) {
                if (StyleOmega.encrypt(mOldString).equals(mCurrentUser.getUserPassword())) {
                    mCurrentUser.setUserPassword(mNewString);
                    mCurrentUser.save();
                    Toast.makeText(mContext, R.string.update_successful, Toast.LENGTH_SHORT).show();
                    this.dismiss();
                } else {
                    Toast.makeText(mContext, R.string.password_invalid, Toast.LENGTH_SHORT).show();
                }
            }
            else{
                Toast.makeText(mContext, R.string.password_error, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
