package com.apiit.shahid.styleomega;

import android.content.Intent;
import android.os.Bundle;

import com.apiit.shahid.styleomega.Models.Product;
import com.apiit.shahid.styleomega.Models.ProductImage;
import com.apiit.shahid.styleomega.Models.ProductTag;
import com.apiit.shahid.styleomega.Models.Size;
import com.apiit.shahid.styleomega.Models.Tag;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orm.SugarRecord;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        if(Product.count(Product.class)<1) {
            try {
                readTags();
                readProducts();
                readSizes();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Intent nextIntent;
        if(StyleOmega.getSharedPreference(this)==null) {
            nextIntent = new Intent(this, LoginActivity.class);
        }
        else{
            nextIntent = new Intent(this, HomeActivity.class);
        }
        startActivity(nextIntent);
        finish();
    }

    public void readSizes() throws IOException{

        ArrayList<Size> productSizes;

        InputStream fileStream = getAssets().open("sizes.json");
        int streamSize = fileStream.available();
        byte[] buffer = new byte[streamSize];

        fileStream.read(buffer);
        fileStream.close();

        String jsonString = new String(buffer, "UTF-8");

        Type founderListType = new TypeToken<ArrayList<Size>>(){}.getType();
        productSizes = new Gson().fromJson(jsonString, founderListType);

        SugarRecord.saveInTx(productSizes);
    }

    public void readProducts() throws IOException {

        ArrayList<Product> productArrayList;
        InputStream fileStream = getAssets().open("products.json");
        int streamSize = fileStream.available();
        byte[] buffer = new byte[streamSize];

        fileStream.read(buffer);
        fileStream.close();

        String jsonString = new String(buffer, "UTF-8");

        Type founderListType = new TypeToken<ArrayList<Product>>(){}.getType();
        productArrayList = new Gson().fromJson(jsonString, founderListType);

        for(Product mProduct: productArrayList){
            mProduct.save();
            for(String mTag: mProduct.getProductTags()){
                ProductTag mProductTag = new ProductTag(mProduct, Tag.getTag(mTag));
                mProductTag.save();
            }
            for(String mImage: mProduct.getImageList()){
                ProductImage mProductImage = new ProductImage(mProduct, mImage);
                mProductImage.save();
            }
        }
    }

    public void readTags() throws IOException {
        ArrayList<Tag> tagArrayList;
        InputStream fileStream = getAssets().open("tags.json");
        int streamSize = fileStream.available();
        byte[] buffer = new byte[streamSize];

        fileStream.read(buffer);
        fileStream.close();

        String jsonString = new String(buffer, "UTF-8");

        Type founderListType = new TypeToken<ArrayList<Tag>>(){}.getType();
        tagArrayList = new Gson().fromJson(jsonString, founderListType);

        SugarRecord.saveInTx(tagArrayList);
    }

}
