package com.apiit.shahid.styleomega;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.apiit.shahid.styleomega.Adapters.InquiryAdapter;
import com.apiit.shahid.styleomega.Adapters.ReviewAdapter;
import com.apiit.shahid.styleomega.Dialog.InquiryDialog;
import com.apiit.shahid.styleomega.Dialog.ReviewDialog;
import com.apiit.shahid.styleomega.Interface.DialogListener;
import com.apiit.shahid.styleomega.Models.Product;
import com.apiit.shahid.styleomega.Models.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductDetailsActivity extends BaseActivity implements DialogListener{

    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;
    @BindView(R.id.addButton) Button mButton;

    private Product mSelectedProduct;
    private User mCurrentUser;
    private int mChoice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        setupActionBar();
        ButterKnife.bind(this);

        mCurrentUser = StyleOmega.getSharedPreference(this);
        Intent mIntent = getIntent();
        mChoice = mIntent.getIntExtra("mDetailChoice",1);
        long mProduct = mIntent.getLongExtra("mSelectedProduct", 1);

        mSelectedProduct = Product.findById(Product.class, mProduct);
        setValues();
    }

    @Override
    public void setValues(){
        switch(mChoice){
            case 1:
                mRecyclerView.setAdapter(new ReviewAdapter(
                        mSelectedProduct.getReviewList(
                                false),this));
                setTitle(R.string.nav_reviews);
                mButton.setText(R.string.add_rating);
                break;
            case 2:
                mRecyclerView.setAdapter(new InquiryAdapter(
                        mSelectedProduct.getInquiryList(
                                false), this));
                setTitle(R.string.nav_inquiries);
                mButton.setText(R.string.inquiries_button);
                break;
        }
    }

    @OnClick(R.id.addButton)
    public void onClick(View mView){
        switch(mChoice){
            case 1: addReview();
            break;
            case 2: addInquiry();
            break;
        }
    }

    public void addReview(){
        Dialog mReviewDialog = new ReviewDialog(this, mCurrentUser, mSelectedProduct, this);
        mReviewDialog.setCanceledOnTouchOutside(false);
        mReviewDialog.show();
    }

    public void addInquiry(){
        Dialog mInquiryDialog = new InquiryDialog(this, mCurrentUser, mSelectedProduct, this);
        mInquiryDialog.setCanceledOnTouchOutside(false);
        mInquiryDialog.show();
    }

}
