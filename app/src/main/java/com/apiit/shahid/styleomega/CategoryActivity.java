package com.apiit.shahid.styleomega;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.apiit.shahid.styleomega.Adapters.ProductAdapter;
import com.apiit.shahid.styleomega.Models.Product;
import com.apiit.shahid.styleomega.Models.Tag;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryActivity extends BaseActivity {

    private static final int COLUMN_NUM = 2;

    @BindView(R.id.taggedList) RecyclerView mTaggedList;

    private Tag mSelectedTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);
        setupActionBar();
        mSelectedTag = Tag.findById(Tag.class, getIntent().getLongExtra("mSelectedTag",0));
        String mTitle = "Tagged "+"'"+mSelectedTag.getProductTag()+"'";
        setTitle(mTitle);
        initTaggedList();
    }

    public void initTaggedList(){
        List<Product> mProductList = mSelectedTag.getTaggedProducts();
        ProductAdapter mTaggedAdapter = new ProductAdapter(mProductList, this);
        GridLayoutManager mTaggedLayout = new GridLayoutManager(this, COLUMN_NUM, LinearLayout.VERTICAL, false);
        mTaggedList.setLayoutManager(mTaggedLayout);
        mTaggedList.setAdapter(mTaggedAdapter);
    }


}
