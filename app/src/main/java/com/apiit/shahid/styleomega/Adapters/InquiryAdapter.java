package com.apiit.shahid.styleomega.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apiit.shahid.styleomega.Models.Inquiry;
import com.apiit.shahid.styleomega.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shahid on 5/3/2018.
 */

public class InquiryAdapter extends RecyclerView.Adapter<InquiryAdapter.ViewHolder>{


    private List<Inquiry> mInquiryList;
    private Context mContext;

    public InquiryAdapter(List<Inquiry> mInquiryList, Context context) {
        this.mInquiryList = mInquiryList;
        mContext = context;
    }

    @NonNull
    @Override
    public InquiryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View customView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_product_inquiry,parent,false);
        return new InquiryAdapter.ViewHolder(customView);
    }

    @Override
    public void onBindViewHolder(@NonNull InquiryAdapter.ViewHolder holder, int position) {
        final Inquiry mInquiry = mInquiryList.get(position);

        holder.mQuestion.setText(mInquiry.getQuestion());
        holder.mAnswer.setText(mInquiry.getAnswer());
        holder.mUser.setText(mInquiry.getUser().getUserFirstName());
        Picasso.get().load(mInquiry.getProduct().getScaledImg()).placeholder(R.drawable.placeholder_img).into(holder.mProduct);
        holder.mDate.setText(mInquiry.getDate());
    }

    @Override
    public int getItemCount() {
        return mInquiryList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.inquiryQuestion) TextView mQuestion;
        @BindView(R.id.inquiryUser) TextView mUser;
        @BindView(R.id.inquiryAnswer) TextView mAnswer;
        @BindView(R.id.inquiryProduct) CircleImageView mProduct;
        @BindView(R.id.inquiryDate) TextView mDate;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


}
