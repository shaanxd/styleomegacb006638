package com.apiit.shahid.styleomega.Models;

import com.orm.SugarRecord;

/**
 * Created by Shahid on 4/28/2018.
 */

public class ProductTag extends SugarRecord {
    Product mProduct;
    Tag mProductTag;

    public ProductTag() {
    }

    public ProductTag(Product product, Tag productTag) {
        mProduct = product;
        mProductTag = productTag;
    }

    public Product getProduct() {
        return mProduct;
    }

    public Tag getProductTag() {
        return mProductTag;
    }

}
