package com.apiit.shahid.styleomega.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apiit.shahid.styleomega.Adapters.FragmentAdapter;
import com.apiit.shahid.styleomega.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shahid on 5/21/2018.
 */

public class OrdersFragment extends BaseFragment{

    @BindView(R.id.orderTab) TabLayout mTabLayout;
    @BindView(R.id.orderPager) ViewPager mViewPager;

    private FragmentAdapter mAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orders, container, false);
        ButterKnife.bind(this, view);

        mAdapter = new FragmentAdapter(getChildFragmentManager());
        mAdapter.addFragment(new ProcessingOrderFragment(),"Processing");
        mAdapter.addFragment(new CancelledOrderFragment(), "Cancelled");

        mViewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        mAdapter.notifyDataSetChanged();
    }
}
