package com.apiit.shahid.styleomega.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Interface.DialogListener;
import com.apiit.shahid.styleomega.Models.Inquiry;
import com.apiit.shahid.styleomega.Models.Product;
import com.apiit.shahid.styleomega.Models.User;
import com.apiit.shahid.styleomega.R;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Shahid on 5/25/2018.
 */

public class InquiryDialog extends Dialog {

    @BindView(R.id.inquiryQuestion) EditText mQuestion;

    private Context mContext;
    private DialogListener mListener;
    private User mCurrentUser;
    private Product mSelectedProduct;

    public InquiryDialog(@NonNull Context context, User currentUser, Product selectedProduct, DialogListener dialogListener) {
        super(context);
        mContext = context;
        mListener = dialogListener;
        mCurrentUser = currentUser;
        mSelectedProduct = selectedProduct;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_inquiry);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.inquiryCancelBtn)
    public void cancelInquiry(View mView){
        this.dismiss();
    }

    @OnClick(R.id.inquirySubmitBtn)
    public void submitInquiry(View mView){
        String mQuestionString = mQuestion.getText().toString();
        if(!(mQuestionString.isEmpty())){
            Inquiry mInquiry = new Inquiry(mSelectedProduct, mCurrentUser,
                    mQuestionString,new Date());
            mInquiry.save();
            mListener.setValues();
            this.dismiss();
        }
        else{
            Toast.makeText(mContext, R.string.empty_error, Toast.LENGTH_SHORT).show();
        }
    }
}
