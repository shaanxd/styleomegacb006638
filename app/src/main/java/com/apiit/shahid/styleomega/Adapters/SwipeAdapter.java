package com.apiit.shahid.styleomega.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.apiit.shahid.styleomega.R;

/**
 * Created by Shahid on 5/19/2018.
 */

public class SwipeAdapter extends PagerAdapter {

    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private int[]mResources = {
            R.drawable.swipe_img1,
            R.drawable.swipe_img2,
            R.drawable.swipe_img3,
            R.drawable.swipe_img4,
            R.drawable.swipe_img5,
            R.drawable.swipe_img6,
            R.drawable.swipe_img7,};

    public SwipeAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_swipe, container, false);
        ImageView mImageView = itemView.findViewById(R.id.swipeImage);
        mImageView.setImageResource(mResources[position]);
        mImageView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                switch (mResources[position]){
                    //Enter on click methods here.
                }
            }
        });
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
