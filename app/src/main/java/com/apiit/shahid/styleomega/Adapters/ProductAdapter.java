package com.apiit.shahid.styleomega.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.apiit.shahid.styleomega.HomeActivity;
import com.apiit.shahid.styleomega.Models.Product;
import com.apiit.shahid.styleomega.ProductActivity;
import com.apiit.shahid.styleomega.R;
import com.apiit.shahid.styleomega.SearchActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shahid on 4/20/2018.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder>{


    private List<Product> mProductArrayList = new ArrayList<>();
    private Context mContext;

    public ProductAdapter(List<Product> productArrayList, Context context) {
        mProductArrayList = productArrayList;
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View customView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_product_layout,parent,false);
        return new ViewHolder(customView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Product product = mProductArrayList.get(position);
        Picasso.get().load(product.getScaledImg()).placeholder(R.drawable.placeholder_img).into(holder.productThumbnail);
        holder.productName.setText(product.getProductName());
        holder.mRatingBar.setRating(product.getAverageRating());
        holder.mProductPrice.setText(String.valueOf(product.getActualPrice()));
        holder.productLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent productIntent = new Intent(mContext, ProductActivity.class);
                productIntent.putExtra("selectedProduct", product.getId());
                mContext.startActivity(productIntent);
                if((mContext.getClass()!= HomeActivity.class) && (mContext.getClass() != SearchActivity.class)){
                    ((Activity)mContext).finish();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProductArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.productThumbnail) ImageView productThumbnail;
        @BindView(R.id.productName) TextView productName;
        @BindView(R.id.productLayout) LinearLayout productLayout;
        @BindView(R.id.productRating) RatingBar mRatingBar;
        @BindView(R.id.productPrice) TextView mProductPrice;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


}
