package com.apiit.shahid.styleomega.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.apiit.shahid.styleomega.Models.ProductImage;
import com.apiit.shahid.styleomega.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Shahid on 5/16/2018.
 */

public class ImageAdapter extends PagerAdapter {

    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private List<ProductImage> mImageList;

    public ImageAdapter(Context context, List<ProductImage> imageList) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mImageList = imageList;
    }

    @Override
    public int getCount() {
        return mImageList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_image, container, false);
        ImageView mImageView = itemView.findViewById(R.id.productImage);
        Picasso.get().load(mImageList.get(position).getImageLink()).placeholder(R.drawable.placeholder_img).into(mImageView);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
