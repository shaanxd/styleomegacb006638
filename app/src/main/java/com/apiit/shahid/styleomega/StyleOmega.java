package com.apiit.shahid.styleomega;

import android.content.Context;
import android.content.SharedPreferences;

import com.apiit.shahid.styleomega.Models.User;
import com.google.gson.Gson;
import com.orm.SugarApp;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Shahid on 4/24/2018.
 */

public class StyleOmega extends SugarApp {

    private static String USER_FILE = "USER_INFO";
    private static String USER_INFO = "LOGGED_USER";

    public static  User getSharedPreference(Context mContext){
        SharedPreferences mPrefs = mContext.getSharedPreferences(USER_FILE,MODE_PRIVATE);
        String json = mPrefs.getString(USER_INFO, "");
        User mUser = new Gson().fromJson(json, User.class);
        if(mUser!=null) {
            return (User.findById(User.class, mUser.getId()));
        }
        return null;
    }

    public static void putSharedPreference(Context mContext, User mUser){
        SharedPreferences mPrefs = mContext.getSharedPreferences(USER_FILE,MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mPrefs.edit();
        String json = new Gson().toJson(mUser);
        mEditor.putString(USER_INFO, json);
        mEditor.apply();
    }

    public static String encrypt(String mPassword) {
        MessageDigest mDigest = null;
        try {
            mDigest = MessageDigest.getInstance("MD5");
            mDigest.update(mPassword.getBytes(),0,mPassword.length());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        assert mDigest != null;
        return(new BigInteger(1,mDigest.digest()).toString(16));
    }

    public static void clearSharedPreferences(Context mContext){
        SharedPreferences mPrefs = mContext.getSharedPreferences(USER_FILE,MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mPrefs.edit();
        mEditor.clear().apply();
    }
}
