package com.apiit.shahid.styleomega.Models;

import com.apiit.shahid.styleomega.StyleOmega;
import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Shahid on 4/22/2018.
 */

public class User extends SugarRecord implements Serializable{


    private String mUserID;
    private String mUserEmail;
    private String mUserFirstName;
    private String mUserLastName;
    private String mUserPassword;
    private String mUserAddress;
    private String mUserCity;
    private String mUserProvince;
    private String mCardNumber;
    private String mSecurityNumber;
    private String mExpirationDate;
    private String mContactNumber;

    public User() {

    }

    public User(String userID, String userEmail, String userFirstName, String userLastName, String userPassword) {
        mUserID = userID;
        mUserEmail = userEmail;
        mUserFirstName = userFirstName;
        mUserLastName = userLastName;
        mUserPassword = StyleOmega.encrypt(userPassword);
        mUserAddress = "N/A";
        mUserCity = "N/A";
        mUserProvince = "N/A";
        mCardNumber = "N/A";
        mSecurityNumber = "N/A";
        mExpirationDate = "N/A";
    }

    public String getUserID() {
        return mUserID;
    }

    public String getUserEmail() {
        return mUserEmail;
    }

    public String getUserFirstName() {
        return mUserFirstName;
    }

    public String getUserLastName() {
        return mUserLastName;
    }

    public String getUserPassword() {
        return mUserPassword;
    }

    public String getUserAddress() {
        return mUserAddress;
    }

    public String getUserCity() {
        return mUserCity;
    }

    public String getUserProvince() {
        return mUserProvince;
    }

    public String getCardNumber() {
        return mCardNumber;
    }

    public String getSecurityNumber() {
        return mSecurityNumber;
    }

    public String getExpirationDate() {
        return mExpirationDate;
    }

    public String getContactNumber() {
        return mContactNumber;
    }

    public void setContactNumber(String contactNumber) {
        mContactNumber = contactNumber;
    }

    public void setUserID(String userID) {
        mUserID = userID;
    }

    public void setUserEmail(String userEmail) {
        mUserEmail = userEmail;
    }

    public void setUserPassword(String userPassword) {
        mUserPassword = StyleOmega.encrypt(userPassword);
    }

    public void setUserAddress(String userAddress) {
        mUserAddress = userAddress;
    }

    public void setUserCity(String userCity) {
        mUserCity = userCity;
    }

    public void setUserProvince(String userProvince) {
        mUserProvince = userProvince;
    }

    public void setCardNumber(String cardNumber) {
        mCardNumber = cardNumber;
    }

    public void setSecurityNumber(String securityNumber) {
        mSecurityNumber = securityNumber;
    }

    public void setExpirationDate(String expirationDate) {
        mExpirationDate = expirationDate;
    }

    public List<Cart> getCartItemList() {
        return Select.from(Cart.class).where(Condition.prop("m_user").eq(this.getId())).and(Condition.prop("m_is_purchased").eq(0)).list();
    }

    public List<Wishlist> getWishlist(){
        return Wishlist.find(Wishlist.class, "m_user = ?", this.getId().toString());
    }

    public List<Review> getReviews(Boolean isLimited){
        List<Review> mReviewList;
        if(isLimited) {
            mReviewList = Select.from(Review.class).where(Condition.prop("m_user").eq(this.getId())).limit("3").list();
        }
        else{
            mReviewList = Select.from(Review.class).where(Condition.prop("m_user").eq(this.getId())).list();
        }
        return mReviewList;
    }

    public List<Inquiry> getInquiries(Boolean isLimited){
        List<Inquiry> mInquiryList;
        if(isLimited) {
            mInquiryList = Select.from(Inquiry.class).where(Condition.prop("m_user").eq(this.getId())).limit("3").list();
        }
        else{
            mInquiryList = Select.from(Inquiry.class).where(Condition.prop("m_user").eq(this.getId())).list();
        }
        return mInquiryList;
    }

    public List<Cart> getPurchased(boolean isLimited){
        List<Cart> mPurchasedList;
        if(isLimited){
            mPurchasedList = Select.from(Cart.class).where(Condition.prop("m_user").eq(this.getId())).and(Condition.prop("m_is_purchased").eq(1)).limit("3").list();
        }
        else{
            mPurchasedList = Select.from(Cart.class).where(Condition.prop("m_user").eq(this.getId())).and(Condition.prop("m_is_purchased").eq(1)).list();
        }
        return mPurchasedList;
    }

    public Cart hasProduct(long productID, long sizeID){
        return Select.from(Cart.class).where(Condition.prop("m_user").eq(this.getId()))
                .and(Condition.prop("m_product").eq(productID))
                .and(Condition.prop("m_size").eq(sizeID))
                .and(Condition.prop("m_is_purchased").eq(0)).first();
    }

    public List<Orders> getOrdersList(boolean isLimited){
        List<Orders> mOrdersList;
        if(isLimited){
            mOrdersList = Select.from(Orders.class)
                    .where(Condition.prop("m_user").eq(this.getId()))
                    .and(Condition.prop("m_is_cancelled").eq(1)).list();
        }
        else{
            mOrdersList = Select.from(Orders.class)
                    .where(Condition.prop("m_user").eq(this.getId()))
                    .and(Condition.prop("m_is_cancelled").eq(0)).list();
        }
        return mOrdersList;
    }

    public static User emailExists(String mEmail){
        return Select.from(User.class).where(Condition.prop("m_user_email").eq(mEmail)).first();
    }

}
