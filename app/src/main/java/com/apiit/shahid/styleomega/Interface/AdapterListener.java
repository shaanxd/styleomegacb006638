package com.apiit.shahid.styleomega.Interface;

/**
 * Created by Shahid on 5/9/2018.
 */

public interface AdapterListener {
    public void updateValues();
}
