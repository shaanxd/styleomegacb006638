package com.apiit.shahid.styleomega.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Interface.DialogListener;
import com.apiit.shahid.styleomega.Models.User;
import com.apiit.shahid.styleomega.R;
import com.apiit.shahid.styleomega.StyleOmega;
import com.orm.query.Condition;
import com.orm.query.Select;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Shahid on 5/25/2018.
 */

public class UserDialog extends Dialog{

    @BindView(R.id.userName) TextView mUsername;
    @BindView(R.id.userEmail) TextView mEmail;
    @BindView(R.id.userPassword) TextView mPassword;

    private User mCurrentUser;
    private DialogListener mDialogListener;
    private Context mContext;

    public UserDialog(@NonNull Context context, User currentUser, DialogListener dialogListener) {
        super(context);
        mContext = context;
        mCurrentUser = currentUser;
        mDialogListener = dialogListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_update_info);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.cancelBtn)
    public void cancelUpdate(View mView){
        this.dismiss();
    }

    @OnClick(R.id.submitBtn)
    public void submitUpdate(View mView){
       String mNameString = mUsername.getText().toString();
       String mEmailString = mEmail.getText().toString();
       String mPasswordString = mPassword.getText().toString();

        if(mNameString.isEmpty()||mEmailString.isEmpty()||mPasswordString.isEmpty()){
            Toast.makeText(mContext,R.string.empty_error,Toast.LENGTH_SHORT).show();
        }
        else{
            if(checkUserExists(mNameString,mEmailString)) {
                if (StyleOmega.encrypt(mPasswordString).equals(mCurrentUser.getUserPassword())) {
                    mCurrentUser.setUserID(mNameString);
                    mCurrentUser.setUserEmail(mEmailString);
                    mCurrentUser.save();
                    Toast.makeText(mContext, R.string.update_successful, Toast.LENGTH_SHORT).show();
                    mDialogListener.setValues();
                    this.dismiss();
                } else {
                    Toast.makeText(mContext, R.string.password_invalid, Toast.LENGTH_SHORT).show();
                }
            }
            else{
                Toast.makeText(mContext, R.string.user_error, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public boolean checkUserExists(String mUserString, String mEmailString){
        User user = Select.from(User.class).where(Condition.prop("m_user_id").eq(mUserString)).or(Condition.prop("m_user_email").eq(mEmailString)).first();
        return user == null;
    }
}
