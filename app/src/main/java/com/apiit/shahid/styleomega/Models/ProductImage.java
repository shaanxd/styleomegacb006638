package com.apiit.shahid.styleomega.Models;

import com.orm.SugarRecord;

/**
 * Created by Shahid on 5/16/2018.
 */

public class ProductImage extends SugarRecord{
    private Product mProduct;
    private String mImageLink;

    public ProductImage() {
    }

    public ProductImage(Product product, String imageLink) {
        mProduct = product;
        mImageLink = imageLink;
    }

    public Product getProduct() {
        return mProduct;
    }

    public String getImageLink() {
        return mImageLink;
    }
}
