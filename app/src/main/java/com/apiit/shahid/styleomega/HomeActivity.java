package com.apiit.shahid.styleomega;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.apiit.shahid.styleomega.Fragments.CategoryFragment;
import com.apiit.shahid.styleomega.Fragments.HomeFragment;
import com.apiit.shahid.styleomega.Fragments.InquiriesFragment;
import com.apiit.shahid.styleomega.Fragments.OrdersFragment;
import com.apiit.shahid.styleomega.Fragments.ReviewsFragment;
import com.apiit.shahid.styleomega.Fragments.UserFragment;
import com.apiit.shahid.styleomega.Fragments.WishlistFragment;
import com.apiit.shahid.styleomega.Models.User;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity {

    @BindView(R.id.activityFrame)FrameLayout mFrameLayout;
    @BindView(R.id.navigationView) NavigationView mNavigationView;
    @BindView(R.id.navigationDrawer) DrawerLayout mDrawerLayout;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.search_view) MaterialSearchView mSearchView;

    User mLoggedUser;

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                try {
                    selectDrawerItem(menuItem);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                }
                return true;
            }
        });
    }

    private void selectDrawerItem(MenuItem item)
            throws IllegalAccessException, InstantiationException {
        Fragment mSelectedFragment;
        Class mFragmentClass = null;

        switch (item.getItemId()){
            case R.id.app_home: mFragmentClass = HomeFragment.class;
                break;
            case R.id.app_wishlist: mFragmentClass = WishlistFragment.class;
                break;
            case R.id.app_account: mFragmentClass = UserFragment.class;
                break;
            case R.id.app_categories: mFragmentClass = CategoryFragment.class;
                break;
            case R.id.app_orders: mFragmentClass = OrdersFragment.class;
                break;
            case R.id.app_inquiries: mFragmentClass = InquiriesFragment.class;
                break;
            case R.id.app_reviews: mFragmentClass = ReviewsFragment.class;
                break;
        }

        if(item.getItemId()!=R.id.app_logout && mFragmentClass!=null) {
            if (getSupportFragmentManager().findFragmentById(
                    R.id.activityFrame).getClass() != mFragmentClass) {
                mSelectedFragment = (Fragment) mFragmentClass.newInstance();
                setUpFragment(mSelectedFragment, item);
            }
            mDrawerLayout.closeDrawers();
        }
        else{
            Intent mLoginIntent = new Intent(
                    this, LoginActivity.class);
            StyleOmega.clearSharedPreferences(this);
            startActivity(mLoginIntent);
            finish();
        }
    }

    private void setUpFragment(Fragment mFragment, MenuItem item){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if(fragmentManager.findFragmentById(R.id.activityFrame)!=null) {
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right);
        }
        fragmentTransaction.replace(R.id.activityFrame, mFragment);
        fragmentTransaction.commit();
        setTitle(item.getTitle());
        item.setChecked(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_home);

        ButterKnife.bind(this);
        mLoggedUser = StyleOmega.getSharedPreference(this);

        setupDrawerContent(mNavigationView);
        setSupportActionBar(mToolbar);
        setupDrawerBar();
        mSearchView.setOnQueryTextListener(mQueryListener);

        View mView = mNavigationView.getHeaderView(0);
        TextView mHeaderText = mView.findViewById(R.id.userName);
        TextView mEmailText = mView.findViewById(R.id.userEmail);

        String mWelcomeText = "Welcome "+mLoggedUser.getUserFirstName()+",";
        mHeaderText.setText(mWelcomeText);
        mEmailText.setText(mLoggedUser.getUserEmail());

        try {
            setUpFragment(HomeFragment.class.newInstance(), mNavigationView.getMenu().findItem(R.id.app_home));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case android.R.id.home: mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.action_cart:
                Intent mCartIntent = new Intent(this, CartActivity.class);
                startActivity(mCartIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed(){
        if(mDrawerLayout.isDrawerOpen(GravityCompat.START)){
            mDrawerLayout.closeDrawers();
        }
        else if(mSearchView.isSearchOpen()){
            mSearchView.closeSearch();
        }
        else {
            if (getSupportFragmentManager().findFragmentById(R.id.activityFrame).getClass() != HomeFragment.class) {
                try {
                    setUpFragment(HomeFragment.class.newInstance(), mNavigationView.getMenu().findItem(R.id.app_home));
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        mSearchView.setMenuItem(item);
        return true;
    }

    MaterialSearchView.OnQueryTextListener mQueryListener
            = new MaterialSearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            Intent mSearchIntent = new Intent(
                    HomeActivity.this, SearchActivity.class);
            mSearchIntent.putExtra("mSearchQuery", query);
            startActivity(mSearchIntent);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }
    };
}
