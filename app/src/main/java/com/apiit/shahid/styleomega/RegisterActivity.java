package com.apiit.shahid.styleomega;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Models.User;
import com.orm.query.Condition;
import com.orm.query.Select;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.userID) EditText mUserID;
    @BindView(R.id.userEmail) EditText mUserEmail;
    @BindView(R.id.firstName) EditText mUserFirstName;
    @BindView(R.id.lastName) EditText mUserLastName;
    @BindView(R.id.userPassword) EditText mUserPassword;
    @BindView(R.id.userPasswordRepeat) EditText mUserPasswordRe;

    String mUserString;
    String mEmailString;
    String mFirstNameString;
    String mLastNameString;
    String mPasswordString;
    String mPasswordReString;

    @OnClick(R.id.registerBtn)
    public void submitRegister(View v) {
        setStrings();
        if(!checkViews()) {
            if (mPasswordString.equals(mPasswordReString)) {
                if(checkUserExists()) {
                    User regUser = new User(mUserString, mEmailString,
                            mFirstNameString, mLastNameString, mPasswordString);
                    regUser.save();
                    finish();
                }
                else{
                    Toast.makeText(RegisterActivity.this, R.string.user_error, Toast.LENGTH_SHORT).show();
                }
            }
            else{
                Toast.makeText(RegisterActivity.this, R.string.password_error, Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(RegisterActivity.this, R.string.empty_error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }

    public boolean checkViews(){
        return mUserString.isEmpty() ||
                mEmailString.isEmpty() ||
                mFirstNameString.isEmpty() ||
                mLastNameString.isEmpty() ||
                mPasswordString.isEmpty() ||
                mPasswordReString.isEmpty();
    }

    public boolean checkUserExists(){
        User user = Select.from(User.class)
                .where(Condition.prop("m_user_id").eq(mUserString))
                .or(Condition.prop("m_user_email").eq(mEmailString)).first();
        return user == null;
    }

    public void setStrings(){
        mUserString = mUserID.getText().toString();
        mEmailString = mUserEmail.getText().toString();
        mFirstNameString = mUserFirstName.getText().toString();
        mLastNameString = mUserLastName.getText().toString();
        mPasswordString = mUserPassword.getText().toString();
        mPasswordReString = mUserPasswordRe.getText().toString();
    }
}
