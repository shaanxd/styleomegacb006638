package com.apiit.shahid.styleomega.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Interface.AdapterListener;
import com.apiit.shahid.styleomega.Models.Cart;
import com.apiit.shahid.styleomega.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Shahid on 4/23/2018.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder>{


    private List<Cart> cartItemList;
    private Context mContext;

    private AdapterListener mAdapterListener;

    public CartAdapter(List<Cart> cartItemList, Context context, Activity mActivity) {
        this.cartItemList = cartItemList;
        mContext = context;
        mAdapterListener = (AdapterListener) mActivity;
    }

    @NonNull
    @Override
    public CartAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View customView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_cart_layout,parent,false);
        return new CartAdapter.ViewHolder(customView);
    }

    @Override
    public void onBindViewHolder(@NonNull final CartAdapter.ViewHolder holder, int position) {
        final Cart mCartItem = cartItemList.get(position);

        Picasso
                .get()
                .load(mCartItem.getProduct().getScaledImg())
                .placeholder(R.drawable.placeholder_img)
                .into(holder.mProductThumbnail);
        holder.mProductName.setText(mCartItem.getProduct().getProductName());

        String mQuantity = "x"+String.valueOf(mCartItem.getQuantity());
        holder.mProductQuantity.setText(mQuantity);
        holder.mTotalPrice.setText(String.valueOf(mCartItem.getTotalPrice()));
        holder.mSizeTag.setText(mCartItem.getSize().getProductSize());
        holder.mProductRating.setRating(mCartItem.getProduct().getAverageRating());

        if(mCartItem.getQuantity()<=mCartItem.getProduct().getAvailableStock()){
            holder.mStockDetails.setText(R.string.in_stock);
            holder.mStockDetails.setTextColor(mContext.getResources().getColor(R.color.colorGreen));
        }
        else{
            holder.mStockDetails.setText(R.string.out_of_stock);
            holder.mStockDetails.setTextColor(mContext.getResources().getColor(R.color.colorRed));
        }
        holder.mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartItemList.remove(mCartItem);
                mCartItem.delete();
                Toast.makeText(mContext, R.string.cart_remove,Toast.LENGTH_SHORT).show();
                notifyItemRemoved(holder.getAdapterPosition());
                mAdapterListener.updateValues();
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartItemList.size();
    }

    public double getTotalPrice(){
        double mTotal = 0;
        for(Cart mCart: cartItemList){
            mTotal += mCart.getTotalPrice();
        }
        return mTotal;
    }

    public boolean checkAvailability(){
        for(Cart mItem: cartItemList){
            if(mItem.getProduct().getAvailableStock()<mItem.getQuantity()){
                return false;
            }
        }
        return true;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.productSize) TextView mSizeTag;
        @BindView(R.id.productThumbnail) CircleImageView mProductThumbnail;
        @BindView(R.id.productName) TextView mProductName;
        @BindView(R.id.productQuantity) TextView mProductQuantity;
        @BindView(R.id.totalPrice) TextView mTotalPrice;
        @BindView(R.id.cancelButton) ImageButton mCancelButton;
        @BindView(R.id.productRating) RatingBar mProductRating;
        @BindView(R.id.stockDetails) TextView mStockDetails;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


}
