package com.apiit.shahid.styleomega.Fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apiit.shahid.styleomega.Dialog.PasswordDialog;
import com.apiit.shahid.styleomega.Dialog.ShippingDialog;
import com.apiit.shahid.styleomega.Dialog.UserDialog;
import com.apiit.shahid.styleomega.Interface.DialogListener;
import com.apiit.shahid.styleomega.Models.User;
import com.apiit.shahid.styleomega.R;
import com.apiit.shahid.styleomega.StyleOmega;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Shahid on 4/19/2018.
 */

public class UserFragment extends BaseFragment implements DialogListener{

    @BindView(R.id.userName) TextView mUsername;
    @BindView(R.id.userEmail) TextView mUserEmail;
    @BindView(R.id.firstName) TextView mFirstName;
    @BindView(R.id.lastName) TextView mLastName;

    @BindView(R.id.userAddress) TextView mAddress;
    @BindView(R.id.userCity) TextView mCity;
    @BindView(R.id.userProvince) TextView mProvince;
    @BindView(R.id.contactNumber) TextView mContact;
    @BindView(R.id.shippingUnavailable) TextView mShippingUnavailable;

    @BindView(R.id.shippingAvailable) LinearLayout mShippingAvailable;

    private User mCurrentUser;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user, container, false);
        ButterKnife.bind(this, view);
        mCurrentUser = StyleOmega.getSharedPreference(mContext);
        setValues();
        return view;
    }

    @OnClick(R.id.editPersonal)
    public void setPersonal(){
        Dialog mPersonalDialog = new UserDialog(mContext, mCurrentUser, this);
        mPersonalDialog.setCanceledOnTouchOutside(false);
        mPersonalDialog.show();
    }

    @OnClick(R.id.editPassword)
    public void setPassword(){
        Dialog mPasswordDialog = new PasswordDialog(mContext, mCurrentUser);
        mPasswordDialog.setCanceledOnTouchOutside(false);
        mPasswordDialog.show();
    }

    @OnClick(R.id.editShipping)
    public void setShipping(){
        Dialog mShippingDialog = new ShippingDialog(mContext, mCurrentUser, this);
        mShippingDialog.setCanceledOnTouchOutside(false);
        mShippingDialog.show();
    }

    @Override
    public void setValues() {
        mUsername.setText(mCurrentUser.getUserID());
        mUserEmail.setText(mCurrentUser.getUserEmail());
        mFirstName.setText(mCurrentUser.getUserFirstName());
        mLastName.setText(mCurrentUser.getUserLastName());

        if (mCurrentUser.getUserAddress().equals("N/A")) {
            mShippingUnavailable.setVisibility(View.VISIBLE);
            mShippingAvailable.setVisibility(View.GONE);
        }
        else {
            mShippingUnavailable.setVisibility(View.GONE);
            mShippingAvailable.setVisibility(View.VISIBLE);

            mAddress.setText(mCurrentUser.getUserAddress());
            mCity.setText(mCurrentUser.getUserCity());
            mProvince.setText(mCurrentUser.getUserProvince());
            mContact.setText(mCurrentUser.getContactNumber());
        }
    }
}
