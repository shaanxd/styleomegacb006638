package com.apiit.shahid.styleomega.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Models.User;
import com.apiit.shahid.styleomega.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Shahid on 5/29/2018.
 */

public class ResetDialog extends Dialog {

    @BindView(R.id.generatedString) EditText mCode;
    @BindView(R.id.userPassword) EditText mPassword;
    @BindView(R.id.userPasswordRepeat) EditText mPasswordRepeat;

    private String mGeneratedString;
    private User mUser;
    private Context mContext;

    public ResetDialog(@NonNull Context context, String mGeneratedString, User mUser) {
        super(context);
        this.mContext = context;
        this.mGeneratedString = mGeneratedString;
        this.mUser = mUser;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_reset);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.cancelBtn)
    public void cancelPassword(View mView){
        this.dismiss();
    }

    @OnClick(R.id.submitBtn)
    public void resetPassword(View mView){
        String codeString = mCode.getText().toString();
        String passwordString = mPassword.getText().toString();
        String passwordRepeatString = mPasswordRepeat.getText().toString();

        if(!(codeString.isEmpty()||
                passwordString.isEmpty()||
                passwordRepeatString.isEmpty())) {
            if (codeString.equals(mGeneratedString)) {
                if (passwordString.equals(passwordRepeatString)) {
                    mUser.setUserPassword(passwordString);
                    mUser.save();
                    Toast.makeText(mContext, R.string.update_successful, Toast.LENGTH_SHORT).show();
                    this.dismiss();
                } else {
                    Toast.makeText(mContext, R.string.password_error, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(mContext, R.string.invalid_code, Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(mContext, R.string.empty_error, Toast.LENGTH_SHORT).show();
        }
    }
}
