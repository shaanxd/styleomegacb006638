package com.apiit.shahid.styleomega;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.apiit.shahid.styleomega.Adapters.ImageAdapter;
import com.apiit.shahid.styleomega.Adapters.InquiryAdapter;
import com.apiit.shahid.styleomega.Adapters.ProductAdapter;
import com.apiit.shahid.styleomega.Adapters.ReviewAdapter;
import com.apiit.shahid.styleomega.Adapters.SizeViewAdapter;
import com.apiit.shahid.styleomega.Adapters.TagViewAdapter;
import com.apiit.shahid.styleomega.Dialog.CartDialog;
import com.apiit.shahid.styleomega.Dialog.InquiryDialog;
import com.apiit.shahid.styleomega.Dialog.ReviewDialog;
import com.apiit.shahid.styleomega.Interface.DialogListener;
import com.apiit.shahid.styleomega.Models.Product;
import com.apiit.shahid.styleomega.Models.ProductImage;
import com.apiit.shahid.styleomega.Models.ProductTag;
import com.apiit.shahid.styleomega.Models.Size;
import com.apiit.shahid.styleomega.Models.User;
import com.apiit.shahid.styleomega.Models.Wishlist;
import com.apiit.shahid.styleomega.Views.CustomViewPager;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProductActivity extends BaseActivity implements DialogListener{

    private static final int COLUMN_NUM = 2;

    @BindView(R.id.reviewRating) RatingBar mRatingCard;

    @BindView(R.id.productDescription) TextView mProductDesc;
    @BindView(R.id.productName) TextView mProductName;
    @BindView(R.id.productPrice) TextView mProductPrice;
    @BindView(R.id.averageRating) TextView mAverageRating;

    @BindView(R.id.productImage) CircleImageView mProductImage;

    @BindView(R.id.wishlistToggle) ToggleButton mWishlistToggle;

    @BindView(R.id.tagView) RecyclerView mTagView;
    @BindView(R.id.sizeView) RecyclerView mSizeView;
    @BindView(R.id.recommendedView) RecyclerView mRecommendedView;
    @BindView(R.id.inquiryView) RecyclerView mInquiryView;
    @BindView(R.id.ratingView) RecyclerView mRatingView;

    @BindView(R.id.progressBar1) ProgressBar mProgressBar1;
    @BindView(R.id.progressBar2) ProgressBar mProgressBar2;
    @BindView(R.id.progressBar3) ProgressBar mProgressBar3;
    @BindView(R.id.progressBar4) ProgressBar mProgressBar4;
    @BindView(R.id.progressBar5) ProgressBar mProgressBar5;


    @BindView(R.id.addCardButton) Button mAddCartButton;

    @BindView(R.id.imagePager) CustomViewPager mCustomViewPager;

    private Product mSelectedProduct;
    private User mLoggedUser;

    //Add to favourite listener
    ToggleButton.OnCheckedChangeListener mWishlistListener
            = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Wishlist mWishlistItem =
                    Wishlist.checkWishlist(mSelectedProduct, mLoggedUser);
            if (isChecked && mWishlistItem == null) {
                mWishlistItem = new Wishlist(mSelectedProduct, mLoggedUser);
                mWishlistItem.save();
                Toast.makeText(ProductActivity.this,
                        R.string.fav_successful,Toast.LENGTH_SHORT).show();
            } else {
                mWishlistItem.delete();
                Toast.makeText(ProductActivity.this,
                        R.string.fav_remove,Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        setupActionBar();
        ButterKnife.bind(this);

        mLoggedUser = StyleOmega.getSharedPreference(this);
        mSelectedProduct = Product.findById(Product.class,getIntent()
                .getLongExtra("selectedProduct",0));

        setTitle(mSelectedProduct.getProductName());
        Picasso.get()
                .load(mSelectedProduct.getScaledImg())
                .placeholder(R.drawable.placeholder_img).into(mProductImage);
        mProductDesc.setText(mSelectedProduct.getProductDesc());
        mProductName.setText(mSelectedProduct.getProductName());

        String mPriceString = "$"+String.valueOf(mSelectedProduct.getActualPrice());
        mProductPrice.setText(mPriceString);

        if(!(mSelectedProduct.getAvailableStock()>0)){
            mAddCartButton.setText(R.string.unavailable_product);
            mAddCartButton.setEnabled(false);
        }
        if(Wishlist.checkWishlist(mSelectedProduct,mLoggedUser)!=null){
            mWishlistToggle.setChecked(true);
        }

        setAdapters();
        mWishlistToggle.setOnCheckedChangeListener(mWishlistListener);
    }


    //Set Tag and Size adapters
    public void setAdapters(){

        List<ProductTag> mTagList = mSelectedProduct.getTagList();
        TagViewAdapter mTagAdapter = new TagViewAdapter(mTagList, this);
        mTagView.setAdapter(mTagAdapter);

        List<Size> mSizeList = mSelectedProduct.getSizeList();
        SizeViewAdapter mSizeAdapter = new SizeViewAdapter(mSizeList, this);
        mSizeView.setAdapter(mSizeAdapter);

        List<ProductImage> mImageList = mSelectedProduct.getProductImages();
        mCustomViewPager.setAdapter(new ImageAdapter(this, mImageList));

        mInquiryView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
        mInquiryView.setNestedScrollingEnabled(false);

        mRatingView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false));
        mRatingView.setNestedScrollingEnabled(false);

        mRecommendedView.setLayoutManager(new GridLayoutManager(this, COLUMN_NUM, LinearLayout.VERTICAL, false));
        mRecommendedView.setAdapter(new ProductAdapter(mSelectedProduct.getFirstTag().getRecommendedProducts(true, mSelectedProduct), this));
        mRecommendedView.setNestedScrollingEnabled(false);

        setValues();
    }


    @OnClick(R.id.reviewBtn)
    public void addReview(View v){
        Dialog mReviewDialog = new ReviewDialog(this, mLoggedUser,
                mSelectedProduct, this);
        mReviewDialog.setCanceledOnTouchOutside(false);
        mReviewDialog.show();
    }

    @OnClick(R.id.inquiryBtn)
    public void addInquiry(View v) {
        final Dialog mInquiryDialog = new InquiryDialog(this, mLoggedUser,
                mSelectedProduct, this);
        mInquiryDialog.setCanceledOnTouchOutside(false);
        mInquiryDialog.show();
    }

    @OnClick(R.id.addCardButton)
    public void addCart(View v) {
        Dialog mCartDialog = new CartDialog(this, mLoggedUser, mSelectedProduct);
        mCartDialog.setCanceledOnTouchOutside(false);
        mCartDialog.show();
    }

    @OnClick(R.id.shareButton)
    public void shareProduct(View mView){
        String mString = mSelectedProduct.getProductName()+"\n"
                +mSelectedProduct.getProductDesc()+"\n"
                +mSelectedProduct.getScaledImg();
        Intent mShareIntent = new Intent();
        mShareIntent.setAction(Intent.ACTION_SEND);
        mShareIntent.putExtra(Intent.EXTRA_TEXT, mString);
        mShareIntent.setType("text/plain");
        startActivity(Intent.createChooser(mShareIntent,
                getResources().getText(R.string.share_via)));
    }

    @OnClick({R.id.reviewSeeAllBtn, R.id.inquirySeeAllBtn})
    public void viewDetails(View mView){
        Intent mIntent = new Intent(this, ProductDetailsActivity.class);
        switch(mView.getId()){
            case R.id.reviewSeeAllBtn:
                mIntent.putExtra("mDetailChoice",1);
                break;
            case R.id.inquirySeeAllBtn:
                mIntent.putExtra("mDetailChoice", 2);
                break;
        }
        mIntent.putExtra("mSelectedProduct",mSelectedProduct.getId());
        startActivity(mIntent);
    }


    @Override
    public void setValues() {
        mAverageRating.setText(String.valueOf(mSelectedProduct.getAverageRating()));

        InquiryAdapter mInquiryAdapter = new InquiryAdapter(mSelectedProduct.getInquiryList(true), this);
        mInquiryView.setAdapter(mInquiryAdapter);

        ReviewAdapter mReviewAdapter = new ReviewAdapter(mSelectedProduct.getReviewList(true), this);
        mRatingView.setAdapter(mReviewAdapter);

        mProgressBar1.setProgress(mSelectedProduct.getRating(1));
        mProgressBar2.setProgress(mSelectedProduct.getRating(2));
        mProgressBar3.setProgress(mSelectedProduct.getRating(3));
        mProgressBar4.setProgress(mSelectedProduct.getRating(4));
        mProgressBar5.setProgress(mSelectedProduct.getRating(5));
    }

    @Override
    public void onResume() {
        super.onResume();
        setValues();
    }
}
