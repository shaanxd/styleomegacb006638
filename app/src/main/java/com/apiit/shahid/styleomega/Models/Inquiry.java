package com.apiit.shahid.styleomega.Models;

import com.orm.SugarRecord;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Shahid on 4/30/2018.
 */

public class Inquiry extends SugarRecord {
    private Product mProduct;
    private User mUser;
    private String mQuestion;
    private String mAnswer;
    private String mDate;

    public Inquiry() {
    }

    public String getDate() {
        return mDate;
    }

    public Inquiry(Product product, User user, String question, Date inquiryDate) {
        mProduct = product;
        mUser = user;
        mQuestion = question;
        mAnswer = "Not yet answered.";
        mDate = new SimpleDateFormat("yyyy/MM/dd", Locale.US).format(inquiryDate);
    }

    public Product getProduct() {
        return mProduct;
    }

    public User getUser() {
        return mUser;
    }

    public String getQuestion() {
        return mQuestion;
    }

    public String getAnswer() {
        return mAnswer;
    }

}
