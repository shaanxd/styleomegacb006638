package com.apiit.shahid.styleomega.Models;

import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Shahid on 5/24/2018.
 */

public class Orders extends SugarRecord{

    private User mUser;
    private String mAddress;
    private String mCity;
    private String mProvince;
    private String mCardNumber;
    private int mItems;
    private double mTotalPrice;
    private boolean mIsCancelled;
    private String mDate;

    public Orders() {
    }

    public Orders(User user, String address, String city, String province, String cardNumber, int items, double totalPrice, Date orderDate) {
        mUser = user;
        mAddress = address;
        mCity = city;
        mProvince = province;
        mCardNumber = cardNumber;
        mItems = items;
        mTotalPrice = totalPrice;
        mIsCancelled = false;
        mDate = new SimpleDateFormat("yyyy/MM/dd", Locale.US).format(orderDate);
    }

    public void setCancelled(boolean cancelled) {
        mIsCancelled = cancelled;
    }

    public User getUser() {
        return mUser;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getCity() {
        return mCity;
    }

    public String getProvince() {
        return mProvince;
    }

    public String getCardNumber() {
        return mCardNumber;
    }

    public int getItems() {
        return mItems;
    }

    public double getTotalPrice() {
        return mTotalPrice;
    }

    public boolean isCancelled() {
        return mIsCancelled;
    }

    public String getDate() {
        return mDate;
    }

    public List<Cart> getItemList(){
        return Select.from(Cart.class).where(Condition.prop("m_order").eq(this.getId())).list();
    }
}
