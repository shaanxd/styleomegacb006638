package com.apiit.shahid.styleomega.Models;

import com.orm.SugarRecord;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Shahid on 4/30/2018.
 */

public class Review extends SugarRecord{

    private Product mProduct;
    private User mUser;
    private String mTitle;
    private String mComment;
    private float mRating;
    private String mDate;

    public Review() {
    }

    public Review(Product product, User user, String title, String comment, float rating, Date reviewDate) {
        mProduct = product;
        mUser = user;
        mTitle = title;
        mComment = comment;
        mRating = rating;
        mDate = new SimpleDateFormat("yyyy/MM/dd", Locale.US).format(reviewDate);
    }

    public String getDate() {
        return mDate;
    }

    public Product getProduct() {
        return mProduct;
    }

    public User getUser() {
        return mUser;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getComment() {
        return mComment;
    }

    public float getRating() {
        return mRating;
    }

}
