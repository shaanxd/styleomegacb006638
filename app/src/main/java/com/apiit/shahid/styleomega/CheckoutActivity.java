package com.apiit.shahid.styleomega;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Adapters.PurchasedAdapter;
import com.apiit.shahid.styleomega.Helper.MailHelper;
import com.apiit.shahid.styleomega.Models.Cart;
import com.apiit.shahid.styleomega.Models.Orders;
import com.apiit.shahid.styleomega.Models.Product;
import com.apiit.shahid.styleomega.Models.User;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckoutActivity extends BaseActivity {

    @BindView (R.id.itemNum) TextView mItemNum;
    @BindView(R.id.subTotal) TextView mSubTotal;
    @BindView(R.id.userAddress) TextView mUserAddress;
    @BindView(R.id.userCity) TextView mUserCity;
    @BindView(R.id.userProvince) TextView mUserProvince;
    @BindView(R.id.contactNumber) TextView mContactNumber;
    @BindView(R.id.cardNumber) TextView mCardNumber;
    @BindView(R.id.expirationDate) TextView mExpirationDate;

    @BindView(R.id.cartItemList) RecyclerView mCheckoutView;

    private User mCurrentUser;

    private String mAddressString;
    private String mCityString;
    private String mProvinceString;
    private String mContactString;
    private String mCardString;
    private String mExpirationString;

    private double mTotalPriceInt;
    private int mItemCountInt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        ButterKnife.bind(this);
        setupActionBar();
        setTitle(R.string.check_out);

        mCurrentUser = StyleOmega.getSharedPreference(this);

        Bundle mBundle = getIntent().getExtras();

        if(mBundle!=null){
            mAddressString = mBundle.getString("mAddress");
            mCityString = mBundle.getString("mCity");
            mProvinceString = mBundle.getString("mProvince");
            mContactString = mBundle.getString("mContact");
            mCardString = mBundle.getString("mCardNumber");
            mExpirationString = mBundle.getString("mExpirationDate");

            mTotalPriceInt = mBundle.getDouble("mTotalPrice");
            mItemCountInt = mBundle.getInt("mItemCount");
        }

        mItemNum.setText(String.valueOf(mItemCountInt));
        String mTotal = "$"+String.valueOf(mTotalPriceInt);
        mSubTotal.setText(mTotal);

        mUserAddress.setText(mAddressString);
        mUserCity.setText(mCityString);
        mUserProvince.setText(mProvinceString);
        mContactNumber.setText(mContactString);
        mCardNumber.setText(mCardString);
        mExpirationDate.setText(mExpirationString);


        initCartView();

    }

    private void initCartView(){
        PurchasedAdapter mPurchasedAdapter = new PurchasedAdapter(
                mCurrentUser.getCartItemList(), this);
        mCheckoutView.setAdapter(mPurchasedAdapter);
        mCheckoutView.setLayoutManager(new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false));
    }

    @OnClick(R.id.cancelBtn)
    public void onCancel(View v) {
        finish();
    }

    @OnClick(R.id.checkOutBtn)
    public void onCheckout(View v) {
        Orders mOrder = new Orders(
                mCurrentUser, mAddressString, mCityString, mProvinceString,
                mCardString, mItemCountInt, mTotalPriceInt, new Date()
        );
        mOrder.save();
        for(Cart mItem: mCurrentUser.getCartItemList()){
            Product mProduct = Product.findById(Product.class,
                    mItem.getProduct().getId());
            mProduct.setAvailableStock(
                    mProduct.getAvailableStock()-mItem.getQuantity());
            mProduct.save();
            mItem.setPurchased(true);
            mItem.setOrder(mOrder);
            mItem.save();
        }
        Toast.makeText(CheckoutActivity.this,
                R.string.purchase_successful, Toast.LENGTH_SHORT).show();
        MailHelper.sendOrderDetails(mCurrentUser, mOrder);
        finish();
    }
}
