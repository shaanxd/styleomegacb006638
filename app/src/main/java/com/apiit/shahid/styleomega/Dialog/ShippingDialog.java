package com.apiit.shahid.styleomega.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Interface.DialogListener;
import com.apiit.shahid.styleomega.Models.User;
import com.apiit.shahid.styleomega.R;
import com.apiit.shahid.styleomega.StyleOmega;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Shahid on 5/25/2018.
 */

public class ShippingDialog extends Dialog {

    @BindView(R.id.userAddress) TextView mAddress;
    @BindView(R.id.userCity) TextView mCity;
    @BindView(R.id.userProvince) TextView mProvince;
    @BindView(R.id.contactNumber) TextView mContactNumber;
    @BindView(R.id.userPassword) TextView mPassword;

    private User mCurrentUser;
    private Context mContext;
    private DialogListener mDialogListener;

    public ShippingDialog(@NonNull Context context, User currentUser, DialogListener dialogListener) {
        super(context);
        mContext = context;
        mCurrentUser = currentUser;
        mDialogListener = dialogListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_update_shipping);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.cancelBtn)
    public void cancelUpdate(View mView){
        this.dismiss();
    }

    @OnClick(R.id.submitBtn)
    public void submitUpdate(View mView){
        String addressString = mAddress.getText().toString();
        String cityString = mCity.getText().toString();
        String provinceString = mProvince.getText().toString();
        String contactString = mContactNumber.getText().toString();
        String passwordString = mPassword.getText().toString();

        if(!(addressString.isEmpty()||cityString.isEmpty()||provinceString.isEmpty()||contactString.isEmpty()||passwordString.isEmpty())){
            if(StyleOmega.encrypt(passwordString).equals(mCurrentUser.getUserPassword())){
                mCurrentUser.setUserAddress(addressString);
                mCurrentUser.setUserCity(cityString);
                mCurrentUser.setUserProvince(provinceString);
                mCurrentUser.setContactNumber(contactString);
                mCurrentUser.save();
                mDialogListener.setValues();
                this.dismiss();
            }
            else{
                Toast.makeText(mContext, R.string.password_invalid, Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(mContext, R.string.empty_error, Toast.LENGTH_SHORT).show();
        }
    }
}
