package com.apiit.shahid.styleomega.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apiit.shahid.styleomega.Models.ProductTag;
import com.apiit.shahid.styleomega.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shahid on 4/28/2018.
 */

public class TagViewAdapter extends RecyclerView.Adapter<TagViewAdapter.ViewHolder>{


    private List<ProductTag> tagList;
    private Context mContext;

    public TagViewAdapter(List<ProductTag> tagList, Context context) {
        this.tagList = tagList;
        mContext = context;
    }

    @NonNull
    @Override
    public TagViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View customView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_tag_layout,parent,false);
        return new TagViewAdapter.ViewHolder(customView);
    }

    @Override
    public void onBindViewHolder(@NonNull TagViewAdapter.ViewHolder holder, int position) {
        final ProductTag tagItem = tagList.get(position);
        holder.tagName.setText(tagItem.getProductTag().getProductTag());
    }

    @Override
    public int getItemCount() {
        return tagList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tagName) TextView tagName;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


}