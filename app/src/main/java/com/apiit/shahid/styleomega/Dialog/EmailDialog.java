package com.apiit.shahid.styleomega.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.apiit.shahid.styleomega.Helper.MailHelper;
import com.apiit.shahid.styleomega.Models.User;
import com.apiit.shahid.styleomega.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Shahid on 5/29/2018.
 */

public class EmailDialog extends Dialog {

    @BindView(R.id.userEmail) EditText mEmail;

    private Context mContext;

    public EmailDialog(@NonNull Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_email);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.cancelBtn)
    public void cancelEmail(View mView){
        this.dismiss();
    }

    @OnClick(R.id.submitBtn)
    public void resetEmail(View mView){
        String emailString = mEmail.getText().toString();
        if(!emailString.isEmpty()) {
            User mUser = User.emailExists(emailString);
            if (mUser != null) {
                String mGeneratedString = Long.toHexString(
                        Double.doubleToLongBits(Math.random()));
                MailHelper.sendPassword(emailString, mGeneratedString);
                Dialog mResetDialog = new ResetDialog(mContext,
                        mGeneratedString, mUser);
                mResetDialog.show();
                this.dismiss();
            } else {
                Toast.makeText(mContext, R.string.email_error, Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(mContext, R.string.empty_error, Toast.LENGTH_SHORT).show();
        }
    }
}
