package com.apiit.shahid.styleomega.Helper;

import com.apiit.shahid.styleomega.Models.Cart;
import com.apiit.shahid.styleomega.Models.Orders;
import com.apiit.shahid.styleomega.Models.User;

import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Created by Shahid on 5/29/2018.
 */

public class MailHelper {

    private static final String SMTP_HOST_NAME = "smtp.gmail.com";
    private static final String SMTP_AUTH_USER = "noreply.styleomega@gmail.com";
    private static final String SMTP_AUTH_PWD  = "styleomega123";

    private static Message message;


    public static void sendEmail(String to, String subject, String msg){
        String from = "noreply.styleomega@gmail.com";

        final String username = SMTP_AUTH_USER;
        final String password = SMTP_AUTH_PWD;
        String host = SMTP_HOST_NAME;

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject(subject);
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(msg, "text/html");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try  {
                        Transport.send(message);
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
            });
            thread.start();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void sendOrderDetails(User mUser, Orders mOrder){
        StringBuilder mMessage = new StringBuilder("Dear " + mUser.getUserFirstName() + ",<br><br>Thank you for shopping at StyleOmega. Given below are your order details.<br><hr>" +
                "<table border='1' cellpadding='10px'>" +
                "<tr>" +
                "<th>Product Name</th>" +
                "<th>Quantity</th>" +
                "<th>Price</th>" +
                "</tr>");
        for(Cart mItem: mOrder.getItemList()){
            String mSubString = "<tr>"
                    +"<td align='center'>"+mItem.getProduct().getProductName()+"</td>"
                    +"<td align='center'>"+mItem.getQuantity()+"</td>"
                    +"<td align='center'>"+mItem.getTotalPrice()+" $</td>" +
                    "<tr>";
            mMessage.append(mSubString);
        }
        mMessage.append("<tr>" + "<td align='center'>Total</td>" + "<td align='center'>")
                .append(mOrder.getItems()).append("</td>")
                .append("<td align='center'>")
                .append(String.valueOf(mOrder.getTotalPrice()))
                .append(" $</td></tr></table><hr>");
        mMessage.append("The items will be shipped to:<br>")
                .append(mUser.getUserFirstName()).append(" ").append(mUser.getUserLastName()).append(",<br>")
                .append(mOrder.getAddress()).append(",<br>")
                .append(mOrder.getCity()).append(",<br>")
                .append(mOrder.getProvince());
        sendEmail(mUser.getUserEmail(), "Order details", mMessage.toString());
    }

    public static void sendPassword(String mEmail, String mGeneratedString){
        String mSubject = "Reset Password";
        String mMessage = "The code to reset password is "+mGeneratedString;
        sendEmail(mEmail, mSubject, mMessage);
    }
}
