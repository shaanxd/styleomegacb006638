package com.apiit.shahid.styleomega.Models;

import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shahid on 4/28/2018.
 */

public class Tag extends SugarRecord {

    private String tagType;
    private String productTag;
    private String tagImage;

    public Tag() {
    }

    public Tag(String tagType, String productTag, String tagImage) {
        this.tagType = tagType;
        this.productTag = productTag;
        this.tagImage = tagImage;
    }

    public String getProductTag() {
        return productTag;
    }

    public String getTagType(){
        return tagType;
    }

    public String getTagImage() {
        return tagImage;
    }

    public List<Product> getRecommendedProducts(Boolean isLimited, Product mSelectedProduct){
        List<Product> mProductList = new ArrayList<>();
        List<ProductTag> mProductTagList;

        if(isLimited) {
            mProductTagList = Select.from(ProductTag.class)
                    .where(Condition.prop("m_product_tag").eq(this.getId()))
                    .and(Condition.prop("m_product").notEq(mSelectedProduct.getId())).limit("4").list();
        }
        else{
            mProductTagList = Select.from(ProductTag.class)
                    .where(Condition.prop("m_product_tag").eq(this.getId()))
                    .and(Condition.prop("m_product").notEq(mSelectedProduct.getId())).list();
        }

        for(ProductTag mProductTag: mProductTagList){
                mProductList.add(mProductTag.getProduct());
        }
        return mProductList;
    }

    public static Tag getTag(String productTag){
        return Tag.findById(Tag.class,Long.parseLong(productTag));
    }

    public static List<Tag> getTagList(String mTag){
        return Select.from(Tag.class)
                .where(Condition.prop("tag_type").eq(mTag)).list();
    }

    public long getProductCount(){
        return Select.from(ProductTag.class).where(Condition.prop("m_product_tag").eq(this.getId())).count();
    }

    public List<Product> getTaggedProducts(){
        List<Product> mProductList = new ArrayList<>();
        List<ProductTag> mTaggedList = Select.from(ProductTag.class).where(Condition.prop("m_product_tag").eq(this.getId())).list();

        for(ProductTag mProductTag: mTaggedList){
            mProductList.add(mProductTag.getProduct());
        }
        return mProductList;
    }

}
