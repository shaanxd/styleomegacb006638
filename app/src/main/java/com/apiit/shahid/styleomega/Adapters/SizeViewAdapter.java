package com.apiit.shahid.styleomega.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apiit.shahid.styleomega.Models.Size;
import com.apiit.shahid.styleomega.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Shahid on 4/29/2018.
 */

public class SizeViewAdapter extends RecyclerView.Adapter<SizeViewAdapter.ViewHolder>{


    private List<Size> sizeList;
    private Context mContext;

    public SizeViewAdapter(List<Size> sizeList, Context context) {
        this.sizeList = sizeList;
        mContext = context;
    }

    @NonNull
    @Override
    public SizeViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View customView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_tag_layout,parent,false);
        return new SizeViewAdapter.ViewHolder(customView);
    }

    @Override
    public void onBindViewHolder(@NonNull SizeViewAdapter.ViewHolder holder, int position) {
        final Size sizeItem = sizeList.get(position);
        holder.tagName.setText(sizeItem.getProductSize());
    }

    @Override
    public int getItemCount() {
        return sizeList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tagName)
        TextView tagName;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


}
